# Name Selection Mobile

Обновление пакетов:
flutter packages get

Запускает built generator:
flutter packages pub run build_runner build --delete-conflicting-outputs

Запуск built value generator в режиме наблюдателя (watch mode):
flutter packages pub run build_runner watch --delete-conflicting-outputs

Для создания билда следует выполнить следующие команды:
flutter clean;
flutter build apk --split-per-abi
