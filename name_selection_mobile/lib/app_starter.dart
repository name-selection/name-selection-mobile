import 'package:built_redux/built_redux.dart';
import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';

import 'core/domain/middlewares/middelware_builder.dart';
// import 'package:name_selection_mobile/injection.dart';
// import 'package:shared_preferences/shared_preferences.dart';

Store<AppState, AppStateBuilder, AppActions> _store;
Store<AppState, AppStateBuilder, AppActions> get store => _store;

startApp() async {
  WidgetsFlutterBinding.ensureInitialized();

  ///проверка пользователя
  // final token = injector.get<SharedPreferences>().getString(PreferenceKeyEnum.apiKey.name);

  _store = new Store(
    reducers,
    AppState(),
    AppActions(),
    middleware: middlewares,
  );

  // if (token != null && token.isNotEmpty) {
  //   print('Есть токен авторизации');
  //   _store.actions.user.setApiKey(token);
  // } else {
  //   print('Нет токена авторизации');
  // }
}
