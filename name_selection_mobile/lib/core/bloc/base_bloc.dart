import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/injection.dart';

abstract class BaseBloc {
  final StoreProvider _storeProvider = injector.get<StoreProvider>();

  Store<AppState, AppStateBuilder, AppActions> get store => _storeProvider.store;

  AppActions get actions => store.actions;

  void init() {}

  void dispose() {}
}
