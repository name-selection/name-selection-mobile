import 'package:built_redux/built_redux.dart';
import 'actions.dart';

part 'app_actions.g.dart';

/// Класс, инициализирующий actions для приложения
abstract class AppActions extends ReduxActions {
  AppActions._();
  factory AppActions() => _$AppActions();

  ActionDispatcher<void> clearState;

  UserActions get userActions;

  NameListActions get nameListActions;

  TopListActions get topListActions;

  NavigationActions get navigationActions;

  RandomNameActions get randomNameActions;

  FavouriteListActions get favouriteListActions;

  NameInfoActions get nameInfoActions;

  CompatibilityActions get compatibilityActions;
}
