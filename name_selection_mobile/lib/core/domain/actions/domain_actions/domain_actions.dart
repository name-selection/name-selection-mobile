/// Здесь прописываются экспорты для actions, которые инициализированы в app_actions.dart
export 'package:name_selection_mobile/core/domain/actions/domain_actions/user_actions.dart';
export 'package:name_selection_mobile/features/name_list/domain/actions/actions.dart';
export 'package:name_selection_mobile/features/top_list/domain/domain.dart';
export 'package:name_selection_mobile/features/navigation/domain/actions/navigation_actions.dart';
export 'package:name_selection_mobile/features/random_name/domain/domain.dart';
export 'package:name_selection_mobile/features/favourite_list/domain/domain.dart';
export 'package:name_selection_mobile/features/name_info/domain/domain.dart';
export 'package:name_selection_mobile/features/compatibility/domain/domain.dart';