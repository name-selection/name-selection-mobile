import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/domain/objects/models/models.dart';

part 'user_actions.g.dart';

abstract class UserActions extends ReduxActions {
  UserActions._();

  factory UserActions() => _$UserActions();

  ActionDispatcher<String> setApiKey;
  ActionDispatcher<UserInfo> setUserInfo;
  ActionDispatcher<void> clear;
}
