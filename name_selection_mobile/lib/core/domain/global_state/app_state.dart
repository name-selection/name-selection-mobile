import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/core.dart';

import 'serializers.dart';

part 'app_state.g.dart';

/// Класс, инициализирующий состояние приложения для всех окон
abstract class AppState implements Built<AppState, AppStateBuilder> {
  AppState._();
  factory AppState([void Function(AppStateBuilder) updates]) {
    return _$AppState((b) => b
      ..appTheme = AppTheme.light
      ..userState = UserState().toBuilder()
      ..navigationState = NavigationState((builder) => builder).toBuilder());
  }

  /// Перечисление состояний окон
  AppTheme get appTheme;

  UserState get userState;

  NavigationState get navigationState;

  NameListState get nameListState;

  TopListState get topListState;

  RandomNameState get randomNameState;

  FavouriteListState get favouriteListState;

  NameInfoState get nameInfoState;

  CompatibilityState get compatibilityState;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(AppState.serializer, this);
  }

  static AppState fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(AppState.serializer, json);
  }

  static Serializer<AppState> get serializer => _$appStateSerializer;
}
