/// Здесь прописываются экспорты для state, которые инициализированы в app_state.dart
/// Должны быть прописаны глобальные состояния (в рамках всего приложения) и локальные (в рамках одного окна)
export 'package:name_selection_mobile/core/domain/global_state/domain_states/user_state.dart';
export 'package:name_selection_mobile/features/navigation/domain/navigaion_state.dart';
export 'package:name_selection_mobile/features/name_list/domain/name_list_state.dart';
export 'package:name_selection_mobile/features/top_list/domain/top_list_state.dart';
export 'package:name_selection_mobile/features/random_name/domain/random_name_state.dart';
export 'package:name_selection_mobile/features/favourite_list/domain/favourite_list_state.dart';
export 'package:name_selection_mobile/features/name_info/domain/name_info_state.dart';
export 'package:name_selection_mobile/features/compatibility/domain/compatibility_state.dart';
