import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/domain/global_state/serializers.dart';

part 'user_state.g.dart';

abstract class UserState implements Built<UserState, UserStateBuilder> {
  UserState._();

  factory UserState([void Function(UserStateBuilder) updates]) = _$UserState;

  @nullable
  String get token;

  @nullable
  String get apiKey;

  @nullable
  UserInfo get userInfo;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(UserState.serializer, this);
  }

  static UserState fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(UserState.serializer, json);
  }

  static Serializer<UserState> get serializer => _$userStateSerializer;
}
