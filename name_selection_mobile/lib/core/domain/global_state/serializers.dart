import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:name_selection_mobile/core/domain/objects/objects.dart';

import 'global_state.dart';

part 'serializers.g.dart';

@SerializersFor([AppState, UserState, UserInfo, Name, SimpleType])
final Serializers serializers = (_$serializers.toBuilder()
      ..add(DateTimeSerializer())
      ..addPlugin(StandardJsonPlugin()))
    .build();
