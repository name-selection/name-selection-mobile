import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/utilities/built_redux_rx.dart';
import 'package:name_selection_mobile/features/name_list/name_list.dart';
import 'package:name_selection_mobile/features/navigation/navigation.dart';

import '../../../injection.dart';

final _nameListEpic = injector.get<NameListEpic>();
final _topListEpic = injector.get<TopListEpic>();
final _randomNameEpic = injector.get<RandomNameEpic>();
final _nameInfoEpic = injector.get<NameInfoEpic>();
final _compatibilityEpic = injector.get<CompatibilityEpic>();

final List<
    void Function(Action<dynamic>) Function(void Function(Action<dynamic>)) Function(
        MiddlewareApi<AppState, AppStateBuilder, AppActions>)> middlewares = [
  nameListMiddleware().build(),
  topListMiddleware().build(),
  navigationMiddleware().build(),
  randomNameMiddleware().build(),
  nameInfoMiddleware().build(),
  compatibilityMiddleware().build(),
  createEpicMiddleware([
    _nameListEpic.maleNameListEpic,
    _nameListEpic.femaleNameListEpic,
    _topListEpic.topListEpic,
    _randomNameEpic.randomNameEpic,
    _nameInfoEpic.nameInfoEpic,
    _nameInfoEpic.nameLikeEpic,
    _compatibilityEpic.maleNamesEpic,
    _compatibilityEpic.femaleNamesEpic,
    _compatibilityEpic.compatibilityEpic,
  ])
];
