import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'gender_type_enum.g.dart';

class GenderTypeEnum extends EnumClass {
  static const GenderTypeEnum male = _$male;
  static const GenderTypeEnum female = _$female;
  static const GenderTypeEnum any = _$any;

  const GenderTypeEnum._(String name) : super(name);

  String get ids => _ids[this] ?? (throw StateError('No id for GenderTypeEnum.$name'));

  String get translates => _translates[this] ?? (throw StateError('No translate for GenderTypeEnum.$name'));

  static const _ids = const {
    male: "MALE",
    female: "FEMALE",
    any: "ANY",
  };

  static const _translates = const {
    male: "Мужское",
    female: "Женское",
    any: "Любое",
  };

  static BuiltSet<GenderTypeEnum> get values => _$genderTypeEnumValues;
  static GenderTypeEnum valueOf(String name) => _$genderTypeEnumValueOf(name);
}
