import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/domain/global_state/serializers.dart';

part 'name.g.dart';

abstract class Name implements Built<Name, NameBuilder> {
  /// Уникальный ключ имени
  @nullable
  String get uuid;

  /// Полное имя
  @nullable
  String get name;

  /// Описание имени
  @nullable
  String get description;

  /// Именины
  @nullable
  String get nameDays;

  /// Варианты имени
  @nullable
  String get nameVariants;

  /// Пол имени
  @nullable
  String get gender;

  /// Кол-во просмотров
  @nullable
  num get likes;

  Name._();

  factory Name([void Function(NameBuilder) updates]) = _$Name;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(Name.serializer, this);
  }

  static Name fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(Name.serializer, json);
  }

  static Serializer<Name> get serializer => _$nameSerializer;
}
