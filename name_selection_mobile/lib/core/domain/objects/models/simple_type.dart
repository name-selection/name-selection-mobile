import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/domain/global_state/serializers.dart';

part 'simple_type.g.dart';

abstract class SimpleType implements Built<SimpleType, SimpleTypeBuilder> {
  /// Уникальный ключ объекта
  @nullable
  String get uuid;

  /// Наименование объекта
  @nullable
  String get name;

  SimpleType._();

  factory SimpleType([void Function(SimpleTypeBuilder) updates]) = _$SimpleType;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(SimpleType.serializer, this);
  }

  static SimpleType fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(SimpleType.serializer, json);
  }

  static Serializer<SimpleType> get serializer => _$simpleTypeSerializer;
}
