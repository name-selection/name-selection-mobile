import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/domain/global_state/serializers.dart';

part 'user_info.g.dart';

abstract class UserInfo implements Built<UserInfo, UserInfoBuilder> {
  @nullable
  String get uuid;

  @nullable
  String get userName;

  @nullable
  String get email;

  UserInfo._();

  factory UserInfo([void Function(UserInfoBuilder) updates]) = _$UserInfo;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(UserInfo.serializer, this);
  }

  static UserInfo fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(UserInfo.serializer, json);
  }

  static Serializer<UserInfo> get serializer => _$userInfoSerializer;
}
