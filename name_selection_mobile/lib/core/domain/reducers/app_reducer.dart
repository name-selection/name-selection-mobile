import 'package:built_redux/built_redux.dart';

import '../domain.dart';

NestedReducerBuilder<AppState, AppStateBuilder, AppState, AppStateBuilder> createAppStateReducer() {
  return NestedReducerBuilder<AppState, AppStateBuilder, AppState, AppStateBuilder>(
    (state) => state,
    (builder) => builder,
  )..add(AppActionsNames.clearState, _clearAppState);
}

// TODO: не забываем расширять список стейтов
void _clearAppState(AppState state, Action<void> action, AppStateBuilder builder) {
  builder
    ..userState = UserState().toBuilder()
    ..favouriteListState = FavouriteListState().toBuilder()
    ..nameInfoState = NameInfoState().toBuilder()
    ..nameListState = NameListState().toBuilder()
    ..topListState = TopListState().toBuilder()
    ..navigationState = NavigationState().toBuilder()
    ..randomNameState = RandomNameState().toBuilder()
    ..compatibilityState = CompatibilityState().toBuilder();
}
