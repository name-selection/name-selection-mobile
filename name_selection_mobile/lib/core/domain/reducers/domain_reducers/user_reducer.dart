import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/domain/domain.dart';

NestedReducerBuilder<AppState, AppStateBuilder, UserState, UserStateBuilder> createUserReducer() {
  return NestedReducerBuilder<AppState, AppStateBuilder, UserState, UserStateBuilder>(
    (state) => state.userState,
    (builder) => builder.userState,
  )
    ..add<String>(UserActionsNames.setApiKey, _setApiKey)
    ..add<UserInfo>(UserActionsNames.setUserInfo, _setUserInfo);
}

void _setApiKey(UserState state, Action<String> action, UserStateBuilder builder) => builder.apiKey = action.payload;

void _setUserInfo(UserState state, Action<UserInfo> action, UserStateBuilder builder) =>
    builder.userInfo.replace(action.payload);
