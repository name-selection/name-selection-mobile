import 'package:built_redux/built_redux.dart';

import '../domain.dart';

final reducerBuilder = ReducerBuilder<AppState, AppStateBuilder>()
  ..combineNested(createUserReducer())
  ..combineNested(createNameListReducer())
  ..combineNested(createTopListReducer())
  ..combineNested(createRandomNameReducer())
  ..combineNested(createFavouriteListReducer())
  ..combineNested(createNameInfoReducer())
  ..combineNested(createCompatibilityReducer())
  ..combineNested(createAppStateReducer());

final reducers = reducerBuilder.build();
