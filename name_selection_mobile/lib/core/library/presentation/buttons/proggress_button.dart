import 'package:flutter/material.dart';

class ProggressButton extends StatelessWidget {
  final Widget child;
  final bool isProgress;
  final Function onPressed;

  const ProggressButton(
      {@required this.child,
      @required this.onPressed,
      this.isProgress = false,
      final key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        onPressed: isProgress ? null : onPressed,
        child: isProgress
            ? Wrap(
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  SizedBox(
                    height: 25,
                    width: 25,
                    child: CircularProgressIndicator(
                      strokeWidth: 4,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    ),
                  ),
                  child
                ],
              )
            : child,
      ),
    );
  }
}
