import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShowSnackBar {
  static const baseInfoDuration = Duration(seconds: 2, milliseconds: 500);
  static const baseWarningDuration = Duration(days: 1);
  static const baseNotificationDuration = Duration(days: 1);

  static void showWarningSnackBar(
      {@required String message, Icon icon = const Icon(Icons.warning_amber_sharp, color: Colors.white)}) {
    if (Get.isSnackbarOpen) Get.back();
    Get.snackbar(null, null,
        messageText: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (icon != null)
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: icon,
                ),
              Flexible(
                child: Text(
                  message,
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ],
          ),
        ),
        borderRadius: 10.0,
        borderColor: Colors.black,
        borderWidth: 0.5,
        backgroundColor: Colors.black87,
        barBlur: 5.0,
        shouldIconPulse: true,
        duration: baseWarningDuration,
        snackPosition: SnackPosition.BOTTOM,
        margin: const EdgeInsets.all(10),
        snackStyle: SnackStyle.FLOATING,
        isDismissible: true,
        dismissDirection: SnackDismissDirection.HORIZONTAL);
  }

  static void showInfoSnackBar({@required String message, Icon icon}) {
    Get.snackbar(null, null,
        messageText: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (icon != null)
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: icon,
                ),
              Flexible(
                child: Text(
                  message,
                  style: TextStyle(color: Colors.white, fontSize: 14.0),
                ),
              ),
            ],
          ),
        ),
        borderRadius: 0.0,
        backgroundColor: Color.fromRGBO(0, 0, 0, 0.55),
        barBlur: 0.0,
        shouldIconPulse: false,
        duration: baseInfoDuration,
        snackPosition: SnackPosition.BOTTOM,
        margin: const EdgeInsets.all(0),
        snackStyle: SnackStyle.FLOATING,
        isDismissible: true,
        dismissDirection: SnackDismissDirection.HORIZONTAL);
  }
}
