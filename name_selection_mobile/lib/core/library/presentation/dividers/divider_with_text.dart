import 'package:flutter/material.dart';

class DividerWithText extends StatelessWidget {
  final String text;
  final Color dividerColor;
  final Color textColor;
  final Widget trailingIcon;

  const DividerWithText({
    this.text,
    this.dividerColor = Colors.black,
    this.textColor = Colors.black,
    this.trailingIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: new Container(
            margin: const EdgeInsets.only(left: 10.0, right: 20.0),
            child: Divider(
              color: dividerColor,
              height: 36,
            ),
          ),
        ),
        Text(
          "$text",
          style: TextStyle(color: textColor),
        ),
        Expanded(
          flex: 5,
          child: new Container(
            margin: const EdgeInsets.only(left: 20.0, right: 10.0),
            child: Divider(
              color: dividerColor,
              height: 36,
            ),
          ),
        ),
      ],
    );
  }
}
