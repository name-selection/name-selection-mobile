import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:name_selection_mobile/injection.dart';

import '../../core.dart';
import 'interceptor.dart';

const contentTypeHeader = 'application/json';

class AuthInterceptor implements Interceptor {
  final StoreProvider _storeProvider = injector.get<StoreProvider>();

  @override
  Request intercept(Request request) {
    debugPrint('${_storeProvider.hashCode}');
    final token = _storeProvider.store.state.userState.token;
    request.headers.update('content-type', (update) => contentTypeHeader, ifAbsent: () => contentTypeHeader);
    request.headers.update('Authorization', (update) => 'Basic $token', ifAbsent: () => 'Basic $token');
    return request;
  }

  @override
  String toString() {
    return '$runtimeType';
  }
}
