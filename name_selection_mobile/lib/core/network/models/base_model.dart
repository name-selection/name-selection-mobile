library base_model;

import 'package:built_value/built_value.dart';

part 'base_model.g.dart';

@BuiltValue(instantiable: false)
abstract class BaseModel {
  @BuiltValueField(wireName: 'httpCode')
  @nullable
  int get httpCode;
}
