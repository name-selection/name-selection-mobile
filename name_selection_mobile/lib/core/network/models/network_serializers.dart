import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:name_selection_mobile/core/core.dart';

part 'network_serializers.g.dart';

@SerializersFor([
  // TODO: add the built values that require serialization
])
final Serializers serializers = (_$serializers.toBuilder()
      ..add(DateTimeSerializer())
      ..addPlugin(StandardJsonPlugin()))
    .build();
