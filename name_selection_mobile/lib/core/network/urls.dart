/// Класс, содержащий в себе адреса для запросов
abstract class Urls {
  // це адреса для емулятора
  // static const String managerUrl = '10.0.2.2:$managerPort';
  static const String managerUrl = 'ec2-13-53-205-219.eu-north-1.compute.amazonaws.com:$managerPort';
  static const String managerPort = '8080';
  static const String prefix = 'api';

  static const String auth = '/$prefix/user/auth';
  static const String maleNameList = '/$prefix/names/males';
  static const String femaleNameList = '/$prefix/names/females';
  // TODO: #18 переделать
  static const String topList = '/$prefix/top';
  static const String maleNamesSimple = '/$prefix/names/simple_males';
  static const String femaleNamesSimple = '/$prefix/names/simple_females';
  static const String randomName = '/$prefix/names/random';
  static const String getNameInfo = '/$prefix/names/find';
  static const String compatibility = '/$prefix/names/compatibility';

  static const String getNameLike = '/$prefix/statistics/like';
}
