import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

ThemeData lightTheme(context) {
  return ThemeData(
      brightness: Brightness.light,
      visualDensity: VisualDensity(vertical: 0.5, horizontal: 0.5),
      primarySwatch: MaterialColor(
        0xffc060ec,
        <int, Color>{
          50: Color(0xfff5e4fb),
          100: Color(0xffe4bdf6),
          200: Color(0xffd290f1),
          300: Color(0xffc060ec),
          400: Color(0xffb136e7),
          500: Color(0xffa000dc),
          600: Color(0xff8e01d6),
          700: Color(0xff7605cf),
          800: Color(0xff5e09c7),
          900: Color(0xff250eba)
        },
      ),
      primaryColor: Color(0xffc060ec),
      primaryColorBrightness: Brightness.light,
      primaryColorLight: Color(0xfff491ff),
      primaryColorDark: Color(0xff8b2eb9),
      canvasColor: Color(0xffffffff),
      accentColor: Color(0xff7300E5),
      accentColorBrightness: Brightness.light,
      scaffoldBackgroundColor: Color(0xffffffff),
      bottomAppBarColor: Color(0xffffffff),
      cardColor: Color(0xffffffff),
      dividerColor: Color(0xffE7E7E7),
      selectedRowColor: Colors.blueGrey,
      unselectedWidgetColor: Color(0xffc060ec),
      disabledColor: Colors.grey,
      buttonTheme: ButtonThemeData(
        textTheme: ButtonTextTheme.primary,
        colorScheme: ColorScheme.light(),
        buttonColor: Color(0xff7300e5),
        splashColor: Colors.white,
      ),
      buttonColor: Color(0xff7300e5),
      secondaryHeaderColor: Colors.grey,
      textSelectionColor: Colors.grey,
      cursorColor: Colors.black,
      textSelectionHandleColor: Colors.blue,
      backgroundColor: Color(0xffa000dc),
      dialogBackgroundColor: Colors.white,
      indicatorColor: Color(0xffffffff),
      hintColor: Colors.grey,
      errorColor: Colors.red,
      toggleableActiveColor: Color(0xff6D42CE),
      textTheme: TextTheme(
        caption: TextStyle(color: Colors.white),
        headline1: TextStyle(color: Colors.white),
        headline2: TextStyle(color: Colors.white),
        headline3: TextStyle(color: Colors.white),
        headline4: TextStyle(color: Colors.white),
        headline5: TextStyle(color: Colors.white),
        headline6: TextStyle(color: Colors.white),
        bodyText1: TextStyle(color: Colors.black, fontFamily: "Roboto", fontSize: 16),
        bodyText2: TextStyle(color: Colors.black, fontFamily: "Roboto", fontSize: 16),
        subtitle1: TextStyle(color: Colors.black, fontFamily: "Roboto", fontSize: 16),
        button: TextStyle(color: Colors.white),
      ),
      primaryTextTheme: TextTheme(
        caption: TextStyle(color: Colors.white),
        headline1: TextStyle(color: Colors.white),
        headline2: TextStyle(color: Colors.white),
        headline3: TextStyle(color: Colors.white),
        headline4: TextStyle(color: Colors.white),
        headline5: TextStyle(color: Colors.white),
        headline6: TextStyle(color: Colors.white),
        bodyText1: TextStyle(color: Colors.black, fontFamily: "Roboto", fontSize: 16),
        bodyText2: TextStyle(color: Colors.black, fontFamily: "Roboto", fontSize: 16),
        subtitle1: TextStyle(color: Colors.black, fontFamily: "Roboto", fontSize: 16),
        button: TextStyle(color: Colors.white),
      ),
      inputDecorationTheme: InputDecorationTheme(
        alignLabelWithHint: true,
        floatingLabelBehavior: FloatingLabelBehavior.auto,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFF770FB7),
            width: 1.5,
          ),
          borderRadius: BorderRadius.circular(6),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 1.5,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 1.5,
          ),
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      primaryIconTheme: IconThemeData(
        color: Colors.white,
      ),
      accentIconTheme: IconThemeData(
        color: Colors.white,
      ),
      platform: TargetPlatform.android,
      materialTapTargetSize: MaterialTapTargetSize.padded,
      applyElevationOverlayColor: true,
      appBarTheme: AppBarTheme(
        elevation: 4.0,
        color: Color(0xffc060ec),
        brightness: Brightness.dark,
      ),
      typography: Typography.material2018(),
      fontFamily: 'ROBOTO',
      splashFactory: InkSplash.splashFactory);
}

ThemeData darkTheme(context) {
  return ThemeData(
      brightness: Brightness.dark,
      visualDensity: VisualDensity(vertical: 0.5, horizontal: 0.5),
      primarySwatch: MaterialColor(
        0xFFF5E0C3,
        <int, Color>{
          50: Color(0x1a5D4524),
          100: Color(0xa15D4524),
          200: Color(0xaa5D4524),
          300: Color(0xaf5D4524),
          400: Color(0x1a483112),
          500: Color(0xa1483112),
          600: Color(0xaa483112),
          700: Color(0xff483112),
          800: Color(0xaf2F1E06),
          900: Color(0xff2F1E06)
        },
      ),
      primaryColor: Color(0xff5D4524),
      primaryColorBrightness: Brightness.dark,
      primaryColorLight: Color(0x1a311F06),
      primaryColorDark: Color(0xff936F3E),
      canvasColor: Color(0xffE09E45),
      accentColor: Color(0xff457BE0),
      accentColorBrightness: Brightness.dark,
      scaffoldBackgroundColor: Color(0xffB5BFD3),
      bottomAppBarColor: Color(0xff6D42CE),
      cardColor: Color(0xaa311F06),
      dividerColor: Color(0x1f6D42CE),
      focusColor: Color(0x1a311F06),
      hoverColor: Color(0xa15D4524),
      highlightColor: Color(0xaf2F1E06),
      splashColor: Color(0xff457BE0),
//  splashFactory: # override create method from  InteractiveInkFeatureFactory
      selectedRowColor: Colors.grey,
      unselectedWidgetColor: Colors.grey.shade400,
      disabledColor: Colors.grey.shade200,
      buttonTheme: ButtonThemeData(
//button themes
          ),
      toggleButtonsTheme: ToggleButtonsThemeData(
//toggle button theme
          ),
      buttonColor: Color(0xff483112),
      secondaryHeaderColor: Colors.grey,
      textSelectionColor: Color(0x1a483112),
      cursorColor: Color(0xff483112),
      textSelectionHandleColor: Color(0xff483112),
      backgroundColor: Color(0xff457BE0),
      dialogBackgroundColor: Colors.white,
      indicatorColor: Color(0xff457BE0),
      hintColor: Colors.grey,
      errorColor: Colors.red,
      toggleableActiveColor: Color(0xff6D42CE),
      textTheme: TextTheme(
//text themes that contrast with card and canvas
          ),
      primaryTextTheme: TextTheme(
//text theme that contrast with primary color
          ),
      accentTextTheme: TextTheme(
//text theme that contrast with accent Color
          ),
      inputDecorationTheme: InputDecorationTheme(
// default values for InputDecorator, TextField, and TextFormField
          ),
      iconTheme: IconThemeData(
//icon themes that contrast with card and canvas
          ),
      primaryIconTheme: IconThemeData(
//icon themes that contrast primary color
          ),
      accentIconTheme: IconThemeData(
//icon themes that contrast accent color
          ),
      sliderTheme: SliderThemeData(
          // slider themes
          ),
      tabBarTheme: TabBarTheme(
          // tab bat theme
          ),
      tooltipTheme: TooltipThemeData(
          // tool tip theme
          ),
      cardTheme: CardTheme(
          // card theme
          ),
      chipTheme: ChipThemeData(
          backgroundColor: Color(0xff2F1E06),
          disabledColor: Color(0xa15D4524),
          shape: StadiumBorder(),
          brightness: Brightness.dark,
          labelPadding: EdgeInsets.all(8),
          labelStyle: TextStyle(),
          padding: EdgeInsets.all(8),
          secondaryLabelStyle: TextStyle(),
          secondarySelectedColor: Colors.white38,
          selectedColor: Colors.white
          // chip theme
          ),
      platform: TargetPlatform.android,
      materialTapTargetSize: MaterialTapTargetSize.padded,
      applyElevationOverlayColor: true,
      pageTransitionsTheme: PageTransitionsTheme(
          //page transition theme
          ),
      appBarTheme: AppBarTheme(
          //app bar theme
          ),
      bottomAppBarTheme: BottomAppBarTheme(
          // bottom app bar theme
          ),
      colorScheme: ColorScheme(
          primary: Color(0xff5D4524),
          primaryVariant: Color(0x1a311F06),
          secondary: Color(0xff457BE0),
          secondaryVariant: Color(0xaa457BE0),
          brightness: Brightness.dark,
          background: Color(0xffB5BFD3),
          error: Colors.red,
          onBackground: Color(0xffB5BFD3),
          onError: Colors.red,
          onPrimary: Color(0xff5D4524),
          onSecondary: Color(0xff457BE0),
          onSurface: Color(0xff457BE0),
          surface: Color(0xff457BE0)),
      snackBarTheme: SnackBarThemeData(
          // snack bar theme
          ),
      dialogTheme: DialogTheme(
          // dialog theme
          ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
          // floating action button theme
          ),
      navigationRailTheme: NavigationRailThemeData(
          // navigation rail theme
          ),
      typography: Typography.material2018(),
      cupertinoOverrideTheme: CupertinoThemeData(
          //cupertino theme
          ),
      bottomSheetTheme: BottomSheetThemeData(
          //bottom sheet theme
          ),
      popupMenuTheme: PopupMenuThemeData(
          //pop menu theme
          ),
      bannerTheme: MaterialBannerThemeData(
          // material banner theme
          ),
      dividerTheme: DividerThemeData(
          //divider, vertical divider theme
          ),
      buttonBarTheme: ButtonBarThemeData(
          // button bar theme
          ),
      fontFamily: 'ROBOTO',
      splashFactory: InkSplash.splashFactory);
}
