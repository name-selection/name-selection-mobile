import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'compatibility_screen_status.g.dart';

class CompatibilityScreenStatus extends EnumClass {
  static const CompatibilityScreenStatus fail = _$fail;
  static const CompatibilityScreenStatus loading = _$loading;
  static const CompatibilityScreenStatus waiting = _$waiting;

  const CompatibilityScreenStatus._(String name) : super(name);

  static BuiltSet<CompatibilityScreenStatus> get values => _$compatibilityScreenStatusValues;
  static CompatibilityScreenStatus valueOf(String name) => _$compatibilityScreenStatusValueOf(name);
}
