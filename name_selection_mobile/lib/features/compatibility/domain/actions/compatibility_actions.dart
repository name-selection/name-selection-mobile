import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../../compatibility.dart';

part 'compatibility_actions.g.dart';

abstract class CompatibilityActions extends ReduxActions {
  CompatibilityActions._();

  factory CompatibilityActions() => _$CompatibilityActions();

  ActionDispatcher<void> maleNamesSimpleRequest;
  ActionDispatcher<void> femaleNamesSimpleRequest;
  ActionDispatcher<CompatibilityRequest> compatibilityRequest;

  ActionDispatcher<MaleNamesResponse> setMaleNamesSimpleResponse;
  ActionDispatcher<FemaleNamesResponse> setFemaleNamesSimpleResponse;
  ActionDispatcher<CompatibilityResponse> setCompatibilityResponse;

  ActionDispatcher<BuiltList<SimpleType>> setMaleNamesList;
  ActionDispatcher<BuiltList<SimpleType>> setFemaleNamesList;
  ActionDispatcher<String> setCompatibility;
  ActionDispatcher<CompatibilityScreenStatus> setCompatibilityScreenStatus;
}
