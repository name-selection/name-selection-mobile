import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../compatibility.dart';

NestedReducerBuilder<AppState, AppStateBuilder, CompatibilityState, CompatibilityStateBuilder> createCompatibilityReducer() {
  return NestedReducerBuilder<AppState, AppStateBuilder, CompatibilityState, CompatibilityStateBuilder>(
    (state) => state.compatibilityState,
    (builder) => builder.compatibilityState,
  )
    ..add(CompatibilityActionsNames.setMaleNamesList, _setMaleNamesList)
    ..add(CompatibilityActionsNames.setFemaleNamesList, _setFemaleNamesList)
    ..add(CompatibilityActionsNames.setCompatibility, _setCompatibility)
    ..add(CompatibilityActionsNames.setCompatibilityScreenStatus, _setCompatibilityScreenStatus);
}

void _setMaleNamesList(CompatibilityState state, Action<BuiltList<SimpleType>> action, CompatibilityStateBuilder builder) {
  builder.maleNamesList.replace(action.payload);
}

void _setFemaleNamesList(CompatibilityState state, Action<BuiltList<SimpleType>> action, CompatibilityStateBuilder builder) {
  builder.femaleNamesList.replace(action.payload);
}

void _setCompatibility(CompatibilityState state, Action<String> action, CompatibilityStateBuilder builder) {
  builder.compatibility = action.payload;
}

void _setCompatibilityScreenStatus(CompatibilityState state, Action<CompatibilityScreenStatus> action, CompatibilityStateBuilder builder) {
  builder.compatibilityScreenStatus = action.payload;
}
