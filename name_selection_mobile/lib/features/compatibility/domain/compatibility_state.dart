import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../compatibility.dart';

part 'compatibility_state.g.dart';

abstract class CompatibilityState implements Built<CompatibilityState, CompatibilityStateBuilder> {
  /// Список мужских имен в виде (uuid, name)
  @nullable
  BuiltList<SimpleType> get maleNamesList;

  /// Список женских имен в виде (uuid, name)
  @nullable
  BuiltList<SimpleType> get femaleNamesList;

  /// Описание совместимости
  @nullable
  String get compatibility;

  /// Статус экрана
  @nullable
  CompatibilityScreenStatus get compatibilityScreenStatus;

  CompatibilityState._();

  factory CompatibilityState([void Function(CompatibilityStateBuilder) updates]) = _$CompatibilityState;
}
