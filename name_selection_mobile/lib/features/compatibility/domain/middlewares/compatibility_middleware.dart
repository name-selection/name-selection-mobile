import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../../compatibility.dart';

MiddlewareBuilder<AppState, AppStateBuilder, AppActions> compatibilityMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
    ..add(CompatibilityActionsNames.maleNamesSimpleRequest, _maleNamesSimpleRequest)
    ..add(CompatibilityActionsNames.setMaleNamesSimpleResponse, _setMaleNamesSimpleResponse)
    ..add(CompatibilityActionsNames.femaleNamesSimpleRequest, _femaleNamesSimpleRequest)
    ..add(CompatibilityActionsNames.setFemaleNamesSimpleResponse, _setFemaleNamesSimpleResponse)
    ..add(CompatibilityActionsNames.compatibilityRequest, _compatibilityRequest)
    ..add(CompatibilityActionsNames.setCompatibilityResponse, _setCompatibilityResponse);
}

void _maleNamesSimpleRequest(MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<void> action) {
  next(action);
}

void _setMaleNamesSimpleResponse(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<MaleNamesResponse> action) {
  next(action);

  final response = action.payload;

  switch (response.httpCode) {
    case 200:
      api.actions.compatibilityActions.setMaleNamesList(response?.maleNames ?? BuiltList<SimpleType>());
      api.actions.compatibilityActions.setCompatibilityScreenStatus(CompatibilityScreenStatus.waiting);
      print('Успешно');
      break;
    default:
      api.actions.compatibilityActions.setCompatibilityScreenStatus(CompatibilityScreenStatus.fail);
      print('Ошибка');
  }
}

void _femaleNamesSimpleRequest(MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<void> action) {
  next(action);
}

void _setFemaleNamesSimpleResponse(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<FemaleNamesResponse> action) {
  next(action);

  final response = action.payload;

  switch (response.httpCode) {
    case 200:
      api.actions.compatibilityActions.setFemaleNamesList(response?.femaleNames ?? BuiltList<SimpleType>());
      api.actions.compatibilityActions.setCompatibilityScreenStatus(CompatibilityScreenStatus.waiting);
      print('Успешно');
      break;
    default:
      api.actions.compatibilityActions.setCompatibilityScreenStatus(CompatibilityScreenStatus.fail);
      print('Ошибка');
  }
}

void _compatibilityRequest(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<CompatibilityRequest> action) {
  next(action);
  api.actions.compatibilityActions.setCompatibilityScreenStatus(CompatibilityScreenStatus.loading);
}

void _setCompatibilityResponse(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<CompatibilityResponse> action) {
  next(action);

  final response = action.payload;

  switch (response.httpCode) {
    case 200:
      api.actions.compatibilityActions.setCompatibility(response?.compatibility ?? 'Нет данных');
      api.actions.compatibilityActions.setCompatibilityScreenStatus(CompatibilityScreenStatus.waiting);
      print('Успешно');
      break;
    default:
      api.actions.compatibilityActions.setCompatibilityScreenStatus(CompatibilityScreenStatus.fail);
      print('Ошибка');
  }
}
