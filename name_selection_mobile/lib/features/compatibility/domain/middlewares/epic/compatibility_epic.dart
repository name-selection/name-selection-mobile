import 'dart:async';

import 'package:built_redux/built_redux.dart';
import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import '../../../compatibility.dart';

@Injectable()
class CompatibilityEpic {
  CompatibilityEpic(this._repository);

  final CompatibilityRepository _repository;

  Stream maleNamesEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == CompatibilityActionsNames.maleNamesSimpleRequest.name)
        .cast<Action<void>>()
        .switchMap((action) {
      return _repository.makeMaleNamesRequest(
        timeout: Duration(seconds: 20),
      );
    }).doOnData((maleNamesResponse) {
      api.actions.compatibilityActions.setMaleNamesSimpleResponse(maleNamesResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }

  Stream femaleNamesEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == CompatibilityActionsNames.femaleNamesSimpleRequest.name)
        .cast<Action<void>>()
        .switchMap((action) {
      return _repository.makeFemaleNamesRequest(
        timeout: Duration(seconds: 20),
      );
    }).doOnData((femaleNamesResponse) {
      api.actions.compatibilityActions.setFemaleNamesSimpleResponse(femaleNamesResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }

  Stream compatibilityEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == CompatibilityActionsNames.compatibilityRequest.name)
        .cast<Action<CompatibilityRequest>>()
        .switchMap((action) {
      final request = action.payload;
      return _repository.makeCompatibilityRequest(
        request,
        timeout: Duration(seconds: 20),
      );
    }).doOnData((compatibilityResponse) {
      api.actions.compatibilityActions.setCompatibilityResponse(compatibilityResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }
}
