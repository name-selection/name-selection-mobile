import 'dart:async';

import 'package:name_selection_mobile/core/core.dart';
import 'package:built_collection/built_collection.dart';

import '../compatibility.dart';

class CompatibilityBloc extends BaseBloc {
  CompatibilityBloc();

  /// Контроллер списка женских имен
  StreamController<BuiltList<SimpleType>> _femaleNamesController;
  StreamSubscription<BuiltList<SimpleType>> _femaleNamesSubscription;
  Stream<BuiltList<SimpleType>> get femaleNamesStream => _femaleNamesController.stream;

  /// Контроллер списка мужских имен
  StreamController<BuiltList<SimpleType>> _maleNamesController;
  StreamSubscription<BuiltList<SimpleType>> _maleNamesSubscription;
  Stream<BuiltList<SimpleType>> get maleNamesStream => _maleNamesController.stream;

  /// Контроллер для экрана
  StreamController<CompatibilityScreenStatus> _screenStatusController;
  StreamSubscription<CompatibilityScreenStatus> _screenStatusSubscription;
  Stream<CompatibilityScreenStatus> get screenStatusStream => _screenStatusController.stream;

  /// Женское имя
  SimpleType _femaleName;

  /// Мужское имя
  SimpleType _maleName;

  @override
  void init() {
    super.init();

    _femaleNamesController = StreamController<BuiltList<SimpleType>>.broadcast();
    _femaleNamesSubscription = store
        .nextSubstate((AppState state) => state.compatibilityState.femaleNamesList)
        .listen((BuiltList<SimpleType> value) {
      _femaleNamesController.sink.add(value);
    });

    _maleNamesController = StreamController<BuiltList<SimpleType>>.broadcast();
    _maleNamesSubscription = store
        .nextSubstate((AppState state) => state.compatibilityState.maleNamesList)
        .listen((BuiltList<SimpleType> value) {
      _maleNamesController.sink.add(value);
    });

    _screenStatusController = StreamController<CompatibilityScreenStatus>.broadcast();
    _screenStatusSubscription = store
        .nextSubstate((AppState state) => state.compatibilityState.compatibilityScreenStatus)
        .listen((CompatibilityScreenStatus value) {
      _screenStatusController.sink.add(value);
    });

    store.actions.compatibilityActions.femaleNamesSimpleRequest();
    store.actions.compatibilityActions.maleNamesSimpleRequest();
  }

  @override
  void dispose() {
    super.dispose();
    _femaleNamesController.close();
    _maleNamesController.close();
    _screenStatusController.close();

    _femaleNamesSubscription.cancel();
    _maleNamesSubscription.cancel();
    _screenStatusSubscription.cancel();
  }

  /// Список женских имен
  List<SimpleType> get femaleItemList => store.state.compatibilityState?.femaleNamesList?.toList() ?? [];

  /// Список мужских имен
  List<SimpleType> get maleItemList => store.state.compatibilityState?.maleNamesList?.toList() ?? [];

  /// Описание совместимости
  String get compatibility => store.state.compatibilityState.compatibility ?? 'Нет данных';

  /// Сохранение выбранного женского имени
  void saveFemaleName(SimpleType femaleName) {
    _femaleName = femaleName;
  }

  /// Сохранение выбранного мужского имени
  void saveMaleName(SimpleType maleName) {
    _maleName = maleName;
  }

  /// Запрос на совместимость имен
  void getCompatibility() {
    final request = CompatibilityRequest((builder) => builder
      ..firstNameUuid = _femaleName.uuid
      ..secondNameUuid = _maleName.uuid);
    store.actions.compatibilityActions.compatibilityRequest(request);
  }
}
