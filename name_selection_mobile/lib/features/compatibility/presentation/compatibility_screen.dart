import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'compatibility_bloc.dart';
import 'compatibility_view.dart';

class CompatibilityScreen extends StatelessWidget {
  const CompatibilityScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (ctx) => CompatibilityBloc()..init(),
      dispose: (ctx, bloc) => bloc.dispose(),
      child: CompatibilityView(),
    );
  }
}
