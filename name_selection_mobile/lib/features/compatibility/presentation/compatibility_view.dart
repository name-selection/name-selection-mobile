import 'package:flutter/material.dart';
import 'package:name_selection_mobile/features/compatibility/compatibility.dart';
import 'package:name_selection_mobile/features/drawer_menu/drawer_menu.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';
import 'package:provider/provider.dart';

import 'compatibility_bloc.dart';
import 'widgets/compatibility_form.dart';

class CompatibilityView extends StatefulWidget {
  const CompatibilityView();

  @override
  _CompatibilityViewState createState() => _CompatibilityViewState();
}

class _CompatibilityViewState extends State<CompatibilityView> {
  CompatibilityBloc get bloc => Provider.of<CompatibilityBloc>(context, listen: false);
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool _isEmptyResult = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        _hideKeyboard(context);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Совместимость имен'),
        ),
        drawer: DrawerMenu(
          selectedRoute: Routes.compatibility,
        ),
        body: StreamBuilder<CompatibilityScreenStatus>(
          stream: bloc.screenStatusStream,
          initialData: CompatibilityScreenStatus.waiting,
          builder: (context, snapshot) {
            final screen = snapshot?.data ?? CompatibilityScreenStatus.waiting;
            switch (screen) {
              case CompatibilityScreenStatus.fail:
                return Container(
                  child: Center(
                    child: Text(
                      'Не удалось загрузить данные',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                );
                break;
              case CompatibilityScreenStatus.loading:
                return Center(
                  child: CircularProgressIndicator(),
                );
                break;
              case CompatibilityScreenStatus.waiting:
              default:
                return SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Stack(
                        alignment: Alignment.bottomCenter,
                        children: <Widget>[
                          Form(key: _formKey, child: CompatibilityForm()),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: RaisedButton(
                              color: Color(0xFF8C5EEC),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  bloc.getCompatibility();
                                  setState(() {
                                    _isEmptyResult = false;
                                  });
                                  _hideKeyboard(context);
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Text(
                                  "Узнать",
                                  style: Theme.of(context).textTheme.headline6.copyWith(fontSize: 20),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Visibility(
                        visible: _isEmptyResult ? false : true,
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0),
                            child: Text(
                              'Результат',
                              style: TextStyle(
                                fontSize: 26.0,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Visibility(
                        visible: _isEmptyResult ? false : true,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Text(
                            bloc.compatibility,
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF8C5EEC),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
            }
          },
        ),
      ),
    );
  }
}
