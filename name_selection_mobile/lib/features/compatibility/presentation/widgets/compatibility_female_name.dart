import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/domain/domain.dart';
import 'package:name_selection_mobile/core/library/presentation/search/search_field.dart';
import 'package:provider/provider.dart';

import '../../compatibility.dart';
import '../compatibility_bloc.dart';

class CompatibilityFemaleName extends StatefulWidget {
  @override
  _CompatibilityFemaleNameState createState() => _CompatibilityFemaleNameState();
}

class _CompatibilityFemaleNameState extends State<CompatibilityFemaleName> {
  CompatibilityBloc get bloc => Provider.of<CompatibilityBloc>(context, listen: false);
  MediaQueryData get media => MediaQuery.of(context);

  final double _titleFontSize = 22.0;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        alignment: Alignment.topCenter,
        children: <Widget>[
          SizedBox(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                ),
                color: const Color(0xFFBF60EC),
              ),
            ),
            height: media.size.height * 0.3,
          ),
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Text(
                  'Женское имя',
                  style: TextStyle(fontSize: _titleFontSize, color: Colors.white),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                child: StreamBuilder<BuiltList<SimpleType>>(
                    stream: bloc.femaleNamesStream,
                    builder: (context, snapshot) {
                      final femaleItemList = snapshot?.data?.toList() ?? bloc.femaleItemList;
                      return SearchField(
                        decoration: textInputDecoration(context: context),
                        items: femaleItemList
                            .map(
                              (item) => SearchFieldItem(
                                guid: item.uuid,
                                name: item.name,
                                onTap: (int index) {
                                  bloc.saveFemaleName(item);
                                },
                              ),
                            )
                            .toList(),
                        prefixIcon: Icon(
                          Icons.search,
                          color: const Color.fromRGBO(102, 102, 102, 0.2),
                        ),
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            return '';
                          }
                          if (femaleItemList
                                  .map((p) => p.name.trim().toLowerCase())
                                  .contains(value.trim().toLowerCase()) ==
                              false) {
                            return '';
                          } else {
                            return null;
                          }
                        },
                      );
                    }),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
