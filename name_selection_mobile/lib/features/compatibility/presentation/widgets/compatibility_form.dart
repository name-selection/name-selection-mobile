import 'package:flutter/material.dart';
import 'package:name_selection_mobile/features/compatibility/presentation/presentation.dart';

class CompatibilityForm extends StatefulWidget {
  @override
  _CompatibilityFormState createState() => _CompatibilityFormState();
}

class _CompatibilityFormState extends State<CompatibilityForm> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15.0, 40.0, 15.0, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CompatibilityFemaleName(),
            CompatibilityMaleName(),
          ],
        ),
      ),
    );
  }
}
