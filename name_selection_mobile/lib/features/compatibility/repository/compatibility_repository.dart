import 'dart:async';
import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/utilities/isolate_manager/isolate_manager_mixin.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import 'endpoints/endpoints.dart';
import 'models/models.dart';

abstract class CompatibilityRepository {
  Stream<MaleNamesResponse> makeMaleNamesRequest({Duration timeout});

  Stream<FemaleNamesResponse> makeFemaleNamesRequest({Duration timeout});

  Stream<CompatibilityResponse> makeCompatibilityRequest(CompatibilityRequest request, {Duration timeout});
}

@Injectable(as: CompatibilityRepository)
class CompatibilityRepositoryImpl with IsolateManagerMixin implements CompatibilityRepository {
  final RestService _restService;
  final UrlFactory _urlFactory;

  CompatibilityRepositoryImpl(this._restService, this._urlFactory);

  /// Запрос для загрузки списка мужских имен в виде (uuid, name)
  @override
  Stream<MaleNamesResponse> makeMaleNamesRequest({Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<MaleNamesResponse>();
    subscribe(inputSubject, outputSubject, maleNamesMapRestBundle);
    _makeMaleNamesRequest(input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeMaleNamesRequest(
      {BehaviorSubject<RestBundle> input, BehaviorSubject<MaleNamesResponse> output, Duration timeout}) async {
    final endpoint = MaleNamesEndpoint();
    var url = _urlFactory.createFor<MaleNamesEndpoint>(endpoint);
    executeRestGetStringRequest(input, output, _restService, url, MaleNamesResponse.serializer, timeout);
  }

  /// Запрос для загрузки списка женских имен в виде (uuid, name)
  @override
  Stream<FemaleNamesResponse> makeFemaleNamesRequest({Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<FemaleNamesResponse>();
    subscribe(inputSubject, outputSubject, femaleNamesMapRestBundle);
    _makeFemaleNamesRequest(input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeFemaleNamesRequest(
      {BehaviorSubject<RestBundle> input, BehaviorSubject<FemaleNamesResponse> output, Duration timeout}) async {
    final endpoint = FemaleNamesEndpoint();
    var url = _urlFactory.createFor<FemaleNamesEndpoint>(endpoint);
    executeRestGetStringRequest(input, output, _restService, url, FemaleNamesResponse.serializer, timeout);
  }

  /// Запрос для получения совместимости
  @override
  Stream<CompatibilityResponse> makeCompatibilityRequest(CompatibilityRequest request,
      {Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<CompatibilityResponse>();
    subscribe(inputSubject, outputSubject, compatibilityMapRestBundle);
    _makeCompatibilityRequest(request, input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeCompatibilityRequest(CompatibilityRequest request,
      {BehaviorSubject<RestBundle> input, BehaviorSubject<CompatibilityResponse> output, Duration timeout}) async {
    final endpoint = CompatibilityEndpoint(request: request);
    var url = _urlFactory.createFor<CompatibilityEndpoint>(endpoint);
    final String requestString = json.encode(request);
    executeRestPostStringRequest(
        input, output, _restService, url, requestString, CompatibilityResponse.serializer, timeout, '', false);
  }
}

MaleNamesResponse maleNamesMapRestBundle(RestBundle bundle) {
  if (bundle.status != 200) {
    return MaleNamesResponse((builder) => builder
      ..httpCode = bundle.status
      ..message = bundle.data.toString());
  }
  try {
    final jsonDecoded = {'maleNames': jsonDecode(utf8.decode(bundle?.bodyBytes ?? 0))};
    MaleNamesResponse response = serializers.deserializeWith(bundle.serializer, jsonDecoded);
    return response.rebuild((builder) => builder.httpCode = bundle.status);
  } catch (err) {
    logger.e('maleNamesMapRestBundle $err');
    return MaleNamesResponse((builder) => builder.httpCode = bundle.status);
  }
}

FemaleNamesResponse femaleNamesMapRestBundle(RestBundle bundle) {
  if (bundle.status != 200) {
    return FemaleNamesResponse((builder) => builder
      ..httpCode = bundle.status
      ..message = bundle.data.toString());
  }
  try {
    final jsonDecoded = {'femaleNames': jsonDecode(utf8.decode(bundle?.bodyBytes ?? 0))};
    FemaleNamesResponse response = serializers.deserializeWith(bundle.serializer, jsonDecoded);
    return response.rebuild((builder) => builder.httpCode = bundle.status);
  } catch (err) {
    logger.e('femaleNamesMapRestBundle $err');
    return FemaleNamesResponse((builder) => builder.httpCode = bundle.status);
  }
}

CompatibilityResponse compatibilityMapRestBundle(RestBundle bundle) {
  if (bundle.status != 200) {
    return CompatibilityResponse((builder) => builder
      ..httpCode = bundle.status
      ..message = bundle.data.toString());
  }
  try {
    final jsonDecoded = {'compatibility': bundle?.data ?? ''};
    CompatibilityResponse response = serializers.deserializeWith(bundle.serializer, jsonDecoded);
    return response.rebuild((builder) => builder.httpCode = bundle.status);
  } catch (err) {
    logger.e('compatibilityMapRestBundle $err');
    return CompatibilityResponse((builder) => builder.httpCode = bundle.status);
  }
}
