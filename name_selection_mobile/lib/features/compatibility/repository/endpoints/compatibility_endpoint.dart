import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/compatibility/compatibility.dart';

class CompatibilityEndpoint implements Endpoint {
  final CompatibilityRequest request;
  CompatibilityEndpoint({this.request});

  @override
  Uri create() {
    final params = {
      "firstNameUuid": request?.firstNameUuid,
      "secondNameUuid": request?.secondNameUuid,
    };
    return Uri.http(
      Urls.managerUrl,
      Urls.compatibility,
      params,
    );
  }
}
