import 'package:name_selection_mobile/core/core.dart';

class FemaleNamesEndpoint implements Endpoint {
  FemaleNamesEndpoint();

  @override
  Uri create() {
    return Uri.http(
      Urls.managerUrl,
      Urls.femaleNamesSimple,
    );
  }
}
