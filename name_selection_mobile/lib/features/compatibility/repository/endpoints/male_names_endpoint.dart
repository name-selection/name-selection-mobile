import 'package:name_selection_mobile/core/core.dart';

class MaleNamesEndpoint implements Endpoint {
  MaleNamesEndpoint();

  @override
  Uri create() {
    return Uri.http(
      Urls.managerUrl,
      Urls.maleNamesSimple,
    );
  }
}
