import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'compatibility_request.g.dart';

abstract class CompatibilityRequest implements Built<CompatibilityRequest, CompatibilityRequestBuilder> {
  /// Уникальный ключ первого имени
  @nullable
  String get firstNameUuid;

  /// Уникальный ключ второго имени
  @nullable
  String get secondNameUuid;

  CompatibilityRequest._();
  factory CompatibilityRequest([void Function(CompatibilityRequestBuilder) updates]) = _$CompatibilityRequest;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(CompatibilityRequest.serializer, this);
  }

  static CompatibilityRequest fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(CompatibilityRequest.serializer, json);
  }

  static Serializer<CompatibilityRequest> get serializer => _$compatibilityRequestSerializer;
}
