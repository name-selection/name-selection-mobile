import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/network/models/base_model.dart';

import 'serializers.dart';

part 'compatibility_response.g.dart';

abstract class CompatibilityResponse implements BaseModel, Built<CompatibilityResponse, CompatibilityResponseBuilder> {
  /// Описание совместимости
  @nullable
  String get compatibility;

  @nullable
  String get message;

  CompatibilityResponse._();
  factory CompatibilityResponse([void Function(CompatibilityResponseBuilder) updates]) = _$CompatibilityResponse;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(CompatibilityResponse.serializer, this);
  }

  static CompatibilityResponse fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(CompatibilityResponse.serializer, json);
  }

  static Serializer<CompatibilityResponse> get serializer => _$compatibilityResponseSerializer;
}
