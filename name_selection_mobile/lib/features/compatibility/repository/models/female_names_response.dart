import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/network/models/base_model.dart';

import 'serializers.dart';

part 'female_names_response.g.dart';

abstract class FemaleNamesResponse implements BaseModel, Built<FemaleNamesResponse, FemaleNamesResponseBuilder> {
  /// Список записей о женских именах
  @nullable
  BuiltList<SimpleType> get femaleNames;

  @nullable
  String get message;

  FemaleNamesResponse._();
  factory FemaleNamesResponse([void Function(FemaleNamesResponseBuilder) updates]) = _$FemaleNamesResponse;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(FemaleNamesResponse.serializer, this);
  }

  static FemaleNamesResponse fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(FemaleNamesResponse.serializer, json);
  }

  static Serializer<FemaleNamesResponse> get serializer => _$femaleNamesResponseSerializer;
}
