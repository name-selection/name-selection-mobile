import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/network/models/base_model.dart';

import 'serializers.dart';

part 'male_names_response.g.dart';

abstract class MaleNamesResponse implements BaseModel, Built<MaleNamesResponse, MaleNamesResponseBuilder> {
  /// Список записей о мужских именах
  @nullable
  BuiltList<SimpleType> get maleNames;

  @nullable
  String get message;

  MaleNamesResponse._();
  factory MaleNamesResponse([void Function(MaleNamesResponseBuilder) updates]) = _$MaleNamesResponse;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(MaleNamesResponse.serializer, this);
  }

  static MaleNamesResponse fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(MaleNamesResponse.serializer, json);
  }

  static Serializer<MaleNamesResponse> get serializer => _$maleNamesResponseSerializer;
}
