import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:name_selection_mobile/core/core.dart';

import 'models.dart';

part 'serializers.g.dart';

@SerializersFor([
  CompatibilityRequest,
  CompatibilityResponse,
  FemaleNamesResponse,
  MaleNamesResponse,
])
final Serializers serializers = (_$serializers.toBuilder()
      ..add(DateTimeSerializer())
      ..addPlugin(StandardJsonPlugin()))
    .build();
