import 'package:flutter/material.dart';
import 'package:name_selection_mobile/features/navigation/data/routes.dart';
import 'package:provider/provider.dart';

import 'presentation.dart';

class DrawerMenu extends StatelessWidget {
  final Routes selectedRoute;

  DrawerMenu({@required this.selectedRoute});

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (ctx) => DrawerMenuBloc()..init(),
      dispose: (ctx, bloc) => bloc.dispose(),
      child: DrawerMenuView(
        selectedRoute: selectedRoute,
      ),
    );
  }
}
