import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/navigation/domain/domain.dart';
import 'package:name_selection_mobile/features/navigation/navigation.dart';

class DrawerMenuBloc extends BaseBloc {
  @override
  void init() {
    super.init();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Переход на список мужских имен
  void navigateToMaleNameList() {
    actions.navigationActions.routeTo(AppRoute(
      (builder) => builder
        ..route = Routes.maleNameList
        ..navigationType = NavigationType.pushReplacement
        ..transitionType = TransitionType.rightSlide,
    ));
  }

  /// Переход на список женских имен
  void navigateToFemaleNameList() {
    actions.navigationActions.routeTo(AppRoute(
      (builder) => builder
        ..route = Routes.femaleNameList
        ..navigationType = NavigationType.pushReplacement
        ..transitionType = TransitionType.rightSlide,
    ));
  }

  /// Переход на список популярных имен
  void navigateToTopList() {
    actions.navigationActions.routeTo(AppRoute(
      (builder) => builder
        ..route = Routes.topList
        ..navigationType = NavigationType.pushReplacement
        ..transitionType = TransitionType.rightSlide,
    ));
  }

  /// Переход на окно случайного имени
  void navigateToRandomName() {
    actions.navigationActions.routeTo(AppRoute(
      (builder) => builder
        ..route = Routes.randomName
        ..navigationType = NavigationType.pushReplacement
        ..transitionType = TransitionType.rightSlide,
    ));
  }

  /// Переход на список избранных имен
  void navigateToFavouriteNameList() {
    actions.navigationActions.routeTo(AppRoute(
      (builder) => builder
        ..route = Routes.favouriteNames
        ..navigationType = NavigationType.pushReplacement
        ..transitionType = TransitionType.rightSlide,
    ));
  }

  /// Переход на совместимость
  void navigateToCompatibility() {
    actions.navigationActions.routeTo(AppRoute(
      (builder) => builder
        ..route = Routes.compatibility
        ..navigationType = NavigationType.pushReplacement
        ..transitionType = TransitionType.rightSlide,
    ));
  }

  /// TODO: сделать при появлении авторизации
  /// Выход из учетной записи и переход на страницу авторизации
  void navigateToExit() {
    // actions.navigationActions.routeTo(AppRoute(
    //   (builder) => builder
    //     ..route = Routes.login
    //     ..navigationType = NavigationType.pushReplacement
    //     ..transitionType = TransitionType.rightSlide,
    // ));
  }
}
