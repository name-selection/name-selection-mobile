import 'package:flutter/material.dart';
import 'package:name_selection_mobile/features/navigation/data/routes.dart';
import 'package:provider/provider.dart';

import 'presentation.dart';
import 'widgets/widgets.dart';

class DrawerMenuView extends StatefulWidget {
  final Routes selectedRoute;

  DrawerMenuView({@required this.selectedRoute});

  @override
  State<StatefulWidget> createState() {
    return new _DrawerMenuViewState(selectedRoute: selectedRoute);
  }
}

class _DrawerMenuViewState extends State<DrawerMenuView> {
  Routes selectedRoute;

  _DrawerMenuViewState({@required this.selectedRoute});

  DrawerMenuBloc get bloc => Provider.of<DrawerMenuBloc>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Material(
            color: Colors.transparent,
            elevation: 10.0,
            child: Container(
              color: Theme.of(context).primaryColor,
              child: DrawerMenuHeader(),
            ),
          ),
          DrawerMenuContent(selectedRoute: selectedRoute),
        ],
      ),
    );
  }
}
