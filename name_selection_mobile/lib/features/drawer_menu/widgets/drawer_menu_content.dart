import 'package:flutter/material.dart';
import 'package:name_selection_mobile/features/navigation/data/routes.dart';
import 'package:provider/provider.dart';

import '../presentation.dart';
import 'widgets.dart';

class DrawerMenuContent extends StatefulWidget {
  final Routes selectedRoute;

  const DrawerMenuContent({Key key, this.selectedRoute}) : super(key: key);

  @override
  _DrawerMenuContentState createState() => _DrawerMenuContentState();
}

class _DrawerMenuContentState extends State<DrawerMenuContent> {
  DrawerMenuBloc get bloc => Provider.of<DrawerMenuBloc>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    final padding = EdgeInsets.symmetric(vertical: 10);

    return Container(
      child: Column(
        children: [
          DrawerMenuItem(
            selected: Routes.maleNameList == widget.selectedRoute,
            text: 'Мужские имена',
            onTap: () => bloc.navigateToMaleNameList(),
            padding: padding,
          ),
          DrawerMenuItem(
            selected: Routes.femaleNameList == widget.selectedRoute,
            text: 'Женские имена',
            onTap: () => bloc.navigateToFemaleNameList(),
            padding: padding,
          ),
          DrawerMenuItem(
            selected: Routes.topList == widget.selectedRoute,
            text: 'Популярные имена',
            onTap: () => bloc.navigateToTopList(),
            padding: padding,
          ),
          DrawerMenuItem(
            selected: Routes.randomName == widget.selectedRoute,
            text: 'Случайное имя',
            onTap: () => bloc.navigateToRandomName(),
            padding: padding,
          ),
          DrawerMenuItem(
            selected: Routes.favouriteNames == widget.selectedRoute,
            text: 'Избранные имена',
            onTap: () => bloc.navigateToFavouriteNameList(),
            padding: padding,
          ),
          DrawerMenuItem(
            selected: Routes.compatibility == widget.selectedRoute,
            text: 'Совместимость',
            onTap: () => bloc.navigateToCompatibility(),
            padding: padding,
          ),
          /// TODO: переместить ниже
          /// Padding(
          //   padding: const EdgeInsets.only(top: 20.0),
          //   child: Divider(
          //     height: 25,
          //     color: Colors.black,
          //     indent: 0,
          //     endIndent: 0,
          //   ),
          // ),
          // DrawerMenuItem(
          //   text: 'Выход',
          //   onTap: () => print('ss'),
          //   padding: padding,
          // ),
        ],
      ),
    );
  }
}
