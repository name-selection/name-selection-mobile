import 'package:flutter/material.dart';

import 'drawer_menu_header_text.dart';

class DrawerMenuHeader extends StatefulWidget {
  final String pathIcon = 'assets/images/logo/logo128.png';

  @override
  _DrawerMenuHeaderState createState() => _DrawerMenuHeaderState();
}

class _DrawerMenuHeaderState extends State<DrawerMenuHeader> {
  MediaQueryData get media => MediaQuery.of(context);

  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      padding: EdgeInsets.all(8.0),
      margin: EdgeInsets.all(0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
            child: Image(
              width: media.size.width / 5.5,
              image: AssetImage(widget.pathIcon),
            ),
          ),
          Expanded(
            child: Container(
              child: DrawerMenuHeaderText(),
            ),
          )
        ],
      ),
    );
  }
}
