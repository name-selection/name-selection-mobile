import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class DrawerMenuHeaderText extends StatefulWidget {
  @override
  _DrawerMenuHeaderTextState createState() => _DrawerMenuHeaderTextState();
}

class _DrawerMenuHeaderTextState extends State<DrawerMenuHeaderText> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        AutoSizeText(
          'Подбор имени',
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Comfortaa',
            fontSize: 42.0,
          ),
          maxLines: 2,
        ),
      ],
    );
  }
}
