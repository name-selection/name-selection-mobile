import 'package:flutter/material.dart';

class DrawerMenuItem extends StatefulWidget {
  final bool selected;
  final String text;
  final Function onTap;
  final EdgeInsets padding;
  final String pathIcon;

  const DrawerMenuItem({Key key, this.selected = false, this.text, this.onTap, this.padding, this.pathIcon})
      : super(key: key);

  @override
  _DrawerMenuItemState createState() => _DrawerMenuItemState();
}

class _DrawerMenuItemState extends State<DrawerMenuItem> {
  MediaQueryData get media => MediaQuery.of(context);

  @override
  Widget build(BuildContext context) {
    if (widget.selected != null && widget.selected) {
      return Container(
        decoration: BoxDecoration(color: Color(0xFFebebeb)),
        child: Padding(
          padding: widget.padding,
          child: Row(
            children: <Widget>[
              widget.pathIcon != null
                  ? Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Image(
                        width: media.size.width / 8.0,
                        image: AssetImage(widget.pathIcon),
                      ),
                    )
                  : Container(),
              Padding(
                padding: EdgeInsets.only(left: 15.0, top: 4.0, bottom: 4.0),
                child: Text(
                  widget.text,
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 20.0,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 0.5,
                  ),
                ),
              )
            ],
          ),
        ),
      );
    } else {
      return GestureDetector(
        child: Container(
          child: Padding(
            padding: widget.padding,
            child: Row(
              children: <Widget>[
                widget.pathIcon != null
                    ? Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Image(
                          width: media.size.width / 8.0,
                          image: AssetImage(widget.pathIcon),
                        ),
                      )
                    : Container(),
                Padding(
                  padding: EdgeInsets.only(left: 15.0, top: 4.0, bottom: 4.0),
                  child: Text(
                    widget.text,
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        onTap: widget.onTap,
      );
    }
  }
}
