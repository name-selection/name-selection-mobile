import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

part 'favourite_list_actions.g.dart';

abstract class FavouriteListActions extends ReduxActions {
  FavouriteListActions._();

  factory FavouriteListActions() => _$FavouriteListActions();

  ActionDispatcher<BuiltList<Name>> setFavouriteList;
}
