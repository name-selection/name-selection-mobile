import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

NestedReducerBuilder<AppState, AppStateBuilder, FavouriteListState, FavouriteListStateBuilder>
    createFavouriteListReducer() {
  return NestedReducerBuilder<AppState, AppStateBuilder, FavouriteListState, FavouriteListStateBuilder>(
    (state) => state.favouriteListState,
    (builder) => builder.favouriteListState,
  )..add(FavouriteListActionsNames.setFavouriteList, _setFavouriteList);
}

void _setFavouriteList(FavouriteListState state, Action<BuiltList<Name>> action, FavouriteListStateBuilder builder) {
  builder.names.replace(action.payload);
}
