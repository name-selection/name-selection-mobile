import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:name_selection_mobile/core/core.dart';

part 'favourite_list_state.g.dart';

abstract class FavouriteListState implements Built<FavouriteListState, FavouriteListStateBuilder> {
  /// Список избранных имен
  @nullable
  BuiltList<Name> get names;

  FavouriteListState._();

  factory FavouriteListState([void Function(FavouriteListStateBuilder) updates]) = _$FavouriteListState;
}
