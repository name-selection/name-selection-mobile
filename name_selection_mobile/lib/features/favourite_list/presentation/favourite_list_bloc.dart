import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_info/presentation/name_info_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavouriteListBloc extends BaseBloc {
  /// Контроллер списка избранных имен
  StreamController<BuiltList<Name>> _favouriteNamesController;
  StreamSubscription<BuiltList<Name>> _favouriteNamesSubscription;
  Stream<BuiltList<Name>> get favouriteNamesStream => _favouriteNamesController.stream;

  @override
  void init() async {
    super.init();
    _favouriteNamesController = StreamController<BuiltList<Name>>.broadcast();
    _favouriteNamesSubscription =
        store.nextSubstate((AppState state) => state.favouriteListState.names).listen((BuiltList<Name> value) {
      _favouriteNamesController.sink.add(value);
    });

    updateNames();
  }

  @override
  void dispose() {
    super.dispose();
    _favouriteNamesController.close();
    _favouriteNamesSubscription.cancel();
  }

  List<Name> get favouriteNamesList => store?.state?.favouriteListState?.names?.toList() ?? [];

  void updateNames() async {
    final prefs = await SharedPreferences.getInstance();
    var uuidList = prefs.getKeys().toList();

    var names = <Name>[];
    for (var i = 0; i < uuidList.length; i++) {
      names.add(
        Name(
          (b) => b
            ..uuid = uuidList[i]
            ..name = prefs.getString(uuidList[i]),
        ),
      );
    }

    _favouriteNamesController.sink.add(names.toBuiltList());
    store.actions.favouriteListActions.setFavouriteList(names.toBuiltList());
  }

  /// Переход на окно с информацией об имени
  void openNameInfo(BuildContext context, {Name name, FutureOr callback}) {
    Route route = MaterialPageRoute(builder: (context) => NameInfoScreen(name: name));
    Navigator.push(context, route).then(callback);
  }
}
