import 'package:flutter/material.dart';
import 'package:name_selection_mobile/features/favourite_list/presentation/presentation.dart';
import 'package:provider/provider.dart';

class FavouriteListScreen extends StatelessWidget {
  const FavouriteListScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (ctx) => FavouriteListBloc()..init(),
      dispose: (ctx, bloc) => bloc.dispose(),
      child: FavouriteListView(),
    );
  }
}
