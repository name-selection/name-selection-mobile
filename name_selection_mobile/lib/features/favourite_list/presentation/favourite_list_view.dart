import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/drawer_menu/drawer_menu.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../favourite_list.dart';

class FavouriteListView extends StatefulWidget {
  @override
  _FavouriteListViewState createState() => _FavouriteListViewState();
}

class _FavouriteListViewState extends State<FavouriteListView> {
  FavouriteListBloc get bloc => Provider.of<FavouriteListBloc>(context, listen: false);

  TextEditingController _searchQueryController = TextEditingController();
  bool _isSearching = false;
  String searchQuery = "";

  FocusNode _textFocusNode;

  /// Для отслеживания избранных имен на иконках
  Future<Set<String>> _favouriteNames;

  @override
  void initState() {
    super.initState();
    _updateData();
    _textFocusNode = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    _textFocusNode.dispose();
  }

  /// Callback при возврате
  FutureOr onGoBack(dynamic value) {
    _updateData();
    setState(() {});
  }
  
  /// Обновление данных
  void _updateData() {
    bloc.updateNames();
    _favouriteNames = SharedPreferences.getInstance().then((SharedPreferences prefs) {
      return (prefs.getKeys());
    });
  }

  /// Удаление имени из избранных
  Future<void> _removeFavourite(Name name) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(name.uuid);
    setState(() {
      _updateData();
    });
  }

  /// Поисковая строка
  Widget _buildSearchField() {
    return TextField(
      focusNode: _textFocusNode,
      controller: _searchQueryController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: "Поиск имени",
        contentPadding: const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
        border: UnderlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(5.0),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      style: TextStyle(color: Colors.black, fontSize: 16.0),
      onChanged: (query) => updateSearchQuery(query),
    );
  }

  /// Кнопка для начала поиска
  List<Widget> _buildActions() {
    if (!_isSearching) {
      return <Widget>[
        IconButton(
          icon: const Icon(Icons.search),
          onPressed: _startSearch,
        ),
      ];
    }
    return null;
  }

  /// Начало поиска
  void _startSearch() {
    ModalRoute.of(context).addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));
    _textFocusNode.requestFocus();

    setState(() {
      _isSearching = true;
    });
  }

  /// Конец поиска
  void _stopSearching() {
    _clearSearchQuery();
    _textFocusNode.unfocus();

    setState(() {
      _isSearching = false;
    });
  }

  /// Обновление поискового запроса
  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
  }

  /// Очистка поискового запроса
  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      updateSearchQuery("");
    });
  }

  /// Скрытие клавиатуры
  void _hideKeyboard() {
    _stopSearching();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: _isSearching ? const BackButton() : null,
        actions: _buildActions(),
        title: _isSearching ? _buildSearchField() : Text('Избранные имена'),
      ),
      drawer: DrawerMenu(selectedRoute: Routes.favouriteNames),
      body: GestureDetector(
        onTap: () => _hideKeyboard(),
        child: StreamBuilder<BuiltList<Name>>(
          stream: bloc.favouriteNamesStream,
          builder: (context, snapshot) {
            final List<Name> searchList = bloc.favouriteNamesList
                .where((e) => e.name.toLowerCase().startsWith(searchQuery.toLowerCase()))
                .toList();
            if (snapshot.data != null && snapshot.data.isNotEmpty) {
              return FutureBuilder<Set<String>>(
                future: _favouriteNames,
                builder: (ctx, snap) {
                  return GroupedListView<Name, String>(
                    shrinkWrap: true,
                    elements: searchList ?? <Name>[],
                    groupBy: (element) => element.name.toLowerCase()[0],
                    groupSeparatorBuilder: (String value) => DividerWithText(
                      text: value.toUpperCase(),
                      textColor: Color(0xff1664A7),
                      dividerColor: Color(0xff1664A7),
                    ),
                    itemBuilder: (context, name) {
                      return ListTile(
                        title: Text(name.name),
                        visualDensity: VisualDensity.compact,
                        trailing: IconButton(
                          icon: Icon(
                            Icons.clear_rounded,
                            color: Colors.red,
                          ),
                          onPressed: () => _removeFavourite(name),
                        ),
                        onTap: () => bloc.openNameInfo(context, name: name, callback: onGoBack),
                      );
                    },
                    separator: Divider(
                      color: Colors.grey,
                      indent: 10,
                      endIndent: 10,
                    ),
                    order: GroupedListOrder.ASC,
                  );
                },
              );
            } else {
              return Container(
                child: Center(
                  child: Text(
                    'Список пуст',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
