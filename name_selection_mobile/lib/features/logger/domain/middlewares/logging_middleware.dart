import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/domain/domain.dart';

import '../../logger.dart';

final application = [
  AppActionsNames.clearState,
];

final login = [];

final home = [];

final taskDetailsActions = [];

final performance = [
  ...db,
];

final db = [
  // DbActionsNames.saveAttachment
];

final ignore = [];
final printPayload = [];

bool printWithPayload(action) => printPayload.map((e) => e.name).contains(action.name);
bool ignorePrint(action) => ignore.map((e) => e.name).contains(action.name);

NextActionHandler loggingMiddleware(MiddlewareApi api) {
  int start, end;

  return (ActionHandler next) {
    return (Action action) {
      assert(() {
        if (performance.map((e) => e.name).contains(action.name)) {
          start = DateTime.now().millisecondsSinceEpoch;
        }
        return true;
      }());

      next(action);

      assert(() {
        if (performance.map((e) => e.name).contains(action.name)) {
          end = DateTime.now().millisecondsSinceEpoch;
          logger
              .d('profile performance ${action.name} ms: ${end - start}  length: ${action.payload.toString().length}');
        }
        return true;
      }());

      assert(() {
        if (printWithPayload(action)) {
          logger.d('Action: ${action.name} ${action.payload.toString()}');
        } else if (!ignorePrint(action)) {
          logger.d('Action: ${action.name}');
        }

        return true;
      }());
    };
  };
}
