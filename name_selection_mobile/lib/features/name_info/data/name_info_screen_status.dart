import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'name_info_screen_status.g.dart';

class NameInfoScreenStatus extends EnumClass {
  static const NameInfoScreenStatus fail = _$fail;
  static const NameInfoScreenStatus loading = _$loading;
  static const NameInfoScreenStatus waiting = _$waiting;

  const NameInfoScreenStatus._(String name) : super(name);

  static BuiltSet<NameInfoScreenStatus> get values => _$nameInfoScreenStatusValues;
  static NameInfoScreenStatus valueOf(String name) => _$nameInfoScreenStatusValueOf(name);
}
