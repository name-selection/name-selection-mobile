import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_info/repository/models/models.dart';

import '../../name_info.dart';

part 'name_info_actions.g.dart';

abstract class NameInfoActions extends ReduxActions {
  NameInfoActions._();

  factory NameInfoActions() => _$NameInfoActions();

  ActionDispatcher<NameInfoRequest> nameInfoRequest;
  ActionDispatcher<NameInfoResponse> setNameInfoResponse;

  ActionDispatcher<NameLikeRequest> nameLikeRequest;
  ActionDispatcher<NameLikeResponse> setNameLikeResponse;
  
  ActionDispatcher<Name> setNameInfo;
  ActionDispatcher<bool> setIsFavouriteField;
  ActionDispatcher<num> setNameLike;
  ActionDispatcher<NameInfoScreenStatus> setNameInfoScreenStatus;
}
