import 'dart:async';

import 'package:built_redux/built_redux.dart';
import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:name_selection_mobile/features/name_info/repository/models/models.dart';
import 'package:name_selection_mobile/features/name_info/repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../../../name_info.dart';

@Injectable()
class NameInfoEpic {
  NameInfoEpic(this._repository);

  final NameInfoRepository _repository;

  Stream nameInfoEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == NameInfoActionsNames.nameInfoRequest.name)
        .cast<Action<NameInfoRequest>>()
        .switchMap((action) {
      final NameInfoRequest request = action.payload;
      return _repository.makeNameInfoRequest(
        request,
        timeout: Duration(seconds: 20),
      );
    }).doOnData((nameInfoResponse) {
      api.actions.nameInfoActions.setNameInfoResponse(nameInfoResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }

  Stream nameLikeEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == NameInfoActionsNames.nameLikeRequest.name)
        .cast<Action<NameLikeRequest>>()
        .switchMap((action) {
      final NameLikeRequest request = action.payload;
      return _repository.makeNameLikeRequest(
        request,
        timeout: Duration(seconds: 20),
      );
    }).doOnData((nameLikeResponse) {
      api.actions.nameInfoActions.setNameLikeResponse(nameLikeResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }
}
