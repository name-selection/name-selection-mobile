import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_info/repository/models/models.dart';

import '../../name_info.dart';

MiddlewareBuilder<AppState, AppStateBuilder, AppActions> nameInfoMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
    ..add(NameInfoActionsNames.nameInfoRequest, _nameInfoRequest)
    ..add(NameInfoActionsNames.setNameInfoResponse, _setNameInfoResponse)
    ..add(NameInfoActionsNames.nameLikeRequest, _nameLikeRequest)
    ..add(NameInfoActionsNames.setNameLikeResponse, _setNameLikeResponse);
}

void _nameInfoRequest(MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<NameInfoRequest> action) {
  next(action);
  api.actions.nameInfoActions.setNameInfoScreenStatus(NameInfoScreenStatus.loading);
}

void _setNameInfoResponse(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<NameInfoResponse> action) {
  next(action);

  final response = action.payload;

  switch (response.httpCode) {
    case 200:
      api.actions.nameInfoActions.setNameInfo(response?.name);
      api.actions.nameInfoActions.setNameInfoScreenStatus(NameInfoScreenStatus.waiting);
      print('Успешно');
      break;
    default:
      api.actions.nameInfoActions.setNameInfoScreenStatus(NameInfoScreenStatus.fail);
      print('Ошибка');
  }
}

void _nameLikeRequest(MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<NameLikeRequest> action) {
  next(action);  
}

void _setNameLikeResponse(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<NameLikeResponse> action) {
  next(action);

  final response = action.payload;

  switch (response.httpCode) {
    case 200:
      api.actions.nameInfoActions.setNameLike(response?.likes);
      print('Успешно');
      break;
    default:
      print('Ошибка');
  }
}
