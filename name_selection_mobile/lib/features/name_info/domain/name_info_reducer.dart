import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../name_info.dart';

NestedReducerBuilder<AppState, AppStateBuilder, NameInfoState, NameInfoStateBuilder> createNameInfoReducer() {
  return NestedReducerBuilder<AppState, AppStateBuilder, NameInfoState, NameInfoStateBuilder>(
    (state) => state.nameInfoState,
    (builder) => builder.nameInfoState,
  )
    ..add(NameInfoActionsNames.setNameInfo, _setNameInfo)
    ..add(NameInfoActionsNames.setIsFavouriteField, _setIsFavouriteField)
    ..add(NameInfoActionsNames.setNameLike, _setNameLike)
    ..add(NameInfoActionsNames.setNameInfoScreenStatus, _setNameInfoScreenStatus);
}

void _setNameInfo(NameInfoState state, Action<Name> action, NameInfoStateBuilder builder) {
  builder.name.replace(action.payload);
}

void _setIsFavouriteField(NameInfoState state, Action<bool> action, NameInfoStateBuilder builder) {
  builder.isFavourite = action.payload;
}

void _setNameLike(NameInfoState state, Action<num> action, NameInfoStateBuilder builder) {
  builder.likes = action.payload;
}

void _setNameInfoScreenStatus(NameInfoState state, Action<NameInfoScreenStatus> action, NameInfoStateBuilder builder) {
  builder.nameInfoScreenStatus = action.payload;
}
