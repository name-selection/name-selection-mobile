import 'package:built_value/built_value.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_info/data/name_info_screen_status.dart';

part 'name_info_state.g.dart';

abstract class NameInfoState implements Built<NameInfoState, NameInfoStateBuilder> {
  /// Информация об имени
  @nullable
  Name get name;

  /// Количество лайков имени
  @nullable
  num get likes;

  /// Является имя избранным или нет
  @nullable
  bool get isFavourite;

  /// Статус экрана
  @nullable
  NameInfoScreenStatus get nameInfoScreenStatus;

  NameInfoState._();

  factory NameInfoState([void Function(NameInfoStateBuilder) updates]) = _$NameInfoState;
}
