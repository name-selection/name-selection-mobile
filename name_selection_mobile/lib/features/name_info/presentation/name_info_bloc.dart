import 'dart:async';

import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_info/repository/models/models.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../name_info.dart';

class NameInfoBloc extends BaseBloc {
  final Name name;
  NameInfoBloc(this.name);

  SharedPreferences _prefs;

  /// Контроллер статуса экрана
  StreamController<NameInfoScreenStatus> _nameInfoScreenStatusController;
  StreamSubscription<NameInfoScreenStatus> _nameInfoScreenStatusSubscription;
  Stream<NameInfoScreenStatus> get nameInfoScreenStatusStream => _nameInfoScreenStatusController.stream;

  /// Контроллер для имени
  StreamController<Name> _nameController;
  StreamSubscription<Name> _nameSubscription;
  Stream<Name> get nameStream => _nameController.stream;

  /// Контроллер для лайков
  StreamController<num> _nameLikeController;
  StreamSubscription<num> _nameLikeSubscription;
  Stream<num> get nameLikeStream => _nameLikeController.stream;

  /// Контроллер для проверки избранного имени
  StreamController<bool> _isFavouriteController;
  StreamSubscription<bool> _isFavouriteSubscription;
  Stream<bool> get isFavouriteStream => _isFavouriteController.stream;

  @override
  void init() async {
    super.init();

    store.actions.nameInfoActions.setNameInfo(name);
    store.actions.nameInfoActions.setNameLike(0);
    /// TODO: убрать запрос на лайки
    // getNameLikes();

    _isFavouriteController = StreamController<bool>.broadcast();
    _isFavouriteSubscription =
        store.nextSubstate((AppState state) => state.nameInfoState.isFavourite).listen((bool value) {
      _isFavouriteController.sink.add(value);
    });

    _nameController = StreamController<Name>.broadcast();
    _nameSubscription = store.nextSubstate((AppState state) => state.nameInfoState.name).listen((Name value) {
      _nameController.sink.add(value);
    });

    _nameLikeController = StreamController<num>.broadcast();
    _nameLikeSubscription = store.nextSubstate((AppState state) => state.nameInfoState.likes).listen((num value) {
      _nameLikeController.sink.add(value);
    });

    _nameInfoScreenStatusController = StreamController<NameInfoScreenStatus>.broadcast();
    _nameInfoScreenStatusSubscription = store
        .nextSubstate((AppState state) => state.nameInfoState.nameInfoScreenStatus)
        .listen((NameInfoScreenStatus value) {
      _nameInfoScreenStatusController.sink.add(value);
    });

    _prefs = await SharedPreferences.getInstance();
    _isFavouriteController.sink.add(checkFavourite(name));

    if (isEmptyName()) {
      getNameInfo();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _isFavouriteController.close();
    _isFavouriteSubscription.cancel();

    _nameController.close();
    _nameLikeController.close();
    _nameSubscription.cancel();
    _nameLikeSubscription.cancel();

    _nameInfoScreenStatusController.close();
    _nameInfoScreenStatusSubscription.cancel();
  }

  /// Проверка на заполненность информации
  bool isEmptyName() {
    /// TODO: дополнять при расширении модели
    if (name == null ||
        name.uuid == null ||
        name.name == null ||
        name.description == null ||
        name.nameVariants == null ||
        name.nameDays == null ||
        name.gender == null ||
        name.likes == null) {
      return true;
    }
    return false;
  }

  /// Получить информацию об имени
  void getNameInfo() {
    final NameInfoRequest request = NameInfoRequest((b) => b..uuid = name.uuid);
    store.actions.nameInfoActions.nameInfoRequest(request);
  }

  /// Получить лайки имени
  void getNameLikes() {
    final NameLikeRequest request = NameLikeRequest((b) => b..uuid = name.uuid);
    store.actions.nameInfoActions.nameLikeRequest(request);
  }

  /// Получить имя
  Name getName() {
    return store.state?.nameInfoState?.name;
  }

  /// Получить пол имени
  String getGender() {
    return store.state?.nameInfoState?.name?.gender;
  }

  /// Получить лайки
  num getLikes() {
    return store.state?.nameInfoState?.likes ?? 0;
  }

  /// Добавление имени в избранный список
  Future<void> addFavourite(Name name) async {
    _prefs.setString(name.uuid, name.name);
    _isFavouriteController.sink.add(true);
  }

  /// Удаление имени из избранного списка
  Future<void> removeFavourite(Name name) async {
    _prefs.remove(name.uuid);
    _isFavouriteController.sink.add(false);
  }

  /// Проверка наличия имени в избранных именах
  bool checkFavourite(Name name) {
    if (_prefs?.containsKey(name.uuid) ?? true) {
      return true;
    }
    return false;
  }
}
