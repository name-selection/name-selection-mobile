import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:provider/provider.dart';

import 'presentation.dart';

class NameInfoScreen extends StatelessWidget {
  final Name name;

  const NameInfoScreen({
    Key key,
    @required this.name,
  })  : assert(name != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (ctx) => NameInfoBloc(name)..init(),
      dispose: (ctx, bloc) => bloc.dispose(),
      child: NameInfoView(name),
    );
  }
}
