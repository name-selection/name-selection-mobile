import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_info/name_info.dart';
import 'package:provider/provider.dart';

import 'name_info_bloc.dart';
import 'widgets/widgets.dart';

class NameInfoView extends StatefulWidget {
  final Name name;
  NameInfoView(this.name);

  @override
  _NameInfoViewState createState() => _NameInfoViewState();
}

class _NameInfoViewState extends State<NameInfoView> {
  NameInfoBloc get bloc => Provider.of<NameInfoBloc>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AutoSizeText(
          'Имя - ${widget.name.name}',
          maxLines: 1,
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: StreamBuilder<NameInfoScreenStatus>(
        stream: bloc.nameInfoScreenStatusStream,
        initialData: NameInfoScreenStatus.waiting,
        builder: (context, snapshot) {
          switch (snapshot.data) {
            case NameInfoScreenStatus.loading:
              return Center(
                child: CircularProgressIndicator(),
              );
              break;
            case NameInfoScreenStatus.fail:
              return Container(
                child: Center(
                  child: Text(
                    'Не удалось загрузить данные',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              );
              break;
            case NameInfoScreenStatus.waiting:
            default:
              return StreamBuilder<Name>(
                stream: bloc.nameStream,
                builder: (context, snapshot) {
                  final data = bloc.getName();
                  return SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        NameInfoTitle(data?.name ?? 'Имя отсутствует'),
                        StreamBuilder<num>(
                          stream: bloc.nameLikeStream,
                          builder: (context, snapshot) {
                            return NameInfoExtra(gender: data?.gender, likes: bloc.getLikes());
                          }
                        ),
                        NameInfoText(
                          title: 'Значение имени',
                          text: data?.description ?? 'Описание отсутствует',
                        ),
                        NameInfoText(
                          title: 'Варианты имени',
                          text: data?.nameVariants ?? 'Не имеется',
                        ),
                        NameInfoText(
                          title: 'Именины',
                          text: data?.nameDays ?? 'Отсутствуют',
                        ),

                        /// TODO: добавить при расширении модели
                        StreamBuilder<bool>(
                            stream: bloc.isFavouriteStream,
                            builder: (context, snapshot) {
                              return ButtonBar(
                                alignment: MainAxisAlignment.center,
                                buttonHeight: 42.0,
                                buttonPadding: EdgeInsets.symmetric(horizontal: 10.0),
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  snapshot.data == false
                                      ? _buildButton('В избранное', Theme.of(context).backgroundColor, () {
                                          bloc.addFavourite(data);
                                        })
                                      : _buildButton('Удалить из избранного', Theme.of(context).backgroundColor, () {
                                          bloc.removeFavourite(data);
                                        })
                                ],
                              );
                            }),
                      ],
                    ),
                  );
                }
              );
          }
        },
      ),
    );
  }
}

Widget _buildButton(String text, Color color, Function func) {
  return RaisedButton(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5.0),
    ),
    elevation: 4.0,
    child: Text(
      text,
      style: TextStyle(
        fontSize: 18.0,
        fontWeight: FontWeight.w500,
        color: Colors.white,
      ),
    ),
    color: color,
    onPressed: () => func(),
  );
}
