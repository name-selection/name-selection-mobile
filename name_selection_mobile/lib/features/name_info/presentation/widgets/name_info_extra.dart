import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class NameInfoExtra extends StatelessWidget {
  final String gender;
  final num likes;
  NameInfoExtra({this.gender, this.likes});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 5.0, 22.0, 0.0),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.85,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: Icon(
                      Icons.remove_red_eye_outlined,
                      color: Color.fromRGBO(122, 122, 122, 0.4),
                    ),
                  ),
                  AutoSizeText(
                    likes.toString(),
                    minFontSize: 14.0,
                    maxFontSize: 18.0,
                    style: TextStyle(
                      color: Color.fromRGBO(122, 122, 122, 0.6),
                    ),
                  ),
                ],
              ),
              AutoSizeText(
                '${gender == 'MALE' ? 'Мужской пол' : 'Женский пол'}',
                minFontSize: 14.0,
                maxFontSize: 18.0,
                style: TextStyle(
                  color: Color.fromRGBO(122, 122, 122, 0.6),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
