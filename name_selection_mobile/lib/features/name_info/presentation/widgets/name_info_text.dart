import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class NameInfoText extends StatelessWidget {
  final String title;
  final String text;
  NameInfoText({this.title, this.text});

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(15.0, 15.0, 0.0, 3.0),
          child: AutoSizeText(
            title,
            minFontSize: 12.0,
            maxFontSize: 16.0,
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(12.0, 5.0, 22.0, 0.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.85,
            decoration: BoxDecoration(
              color: Color(0xFFFFF4FF),
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: AutoSizeText(
                text,
                minFontSize: 14.0,
                maxFontSize: 18.0,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
