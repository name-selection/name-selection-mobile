import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class NameInfoTitle extends StatelessWidget {
  final String title;
  NameInfoTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Center(
        child: AutoSizeText(
          this.title,
          maxLines: 1,
          style: TextStyle(
            fontSize: 36,
            fontWeight: FontWeight.w500,
            color: Theme.of(context).backgroundColor,
          ),
        ),
      ),
    );
  }
}
