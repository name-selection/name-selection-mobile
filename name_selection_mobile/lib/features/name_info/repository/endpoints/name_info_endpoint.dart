import 'package:name_selection_mobile/core/core.dart';
import '../repository.dart';

class NameInfoEndpoint implements Endpoint {
  final NameInfoRequest request;
  NameInfoEndpoint({this.request});

  @override
  Uri create() {
    final params = {'uuid': request.uuid};
    return Uri.http(Urls.managerUrl, Urls.getNameInfo, params);
  }
}
