import 'package:name_selection_mobile/core/core.dart';
import '../repository.dart';

class NameLikeEndpoint implements Endpoint {
  final NameLikeRequest request;
  NameLikeEndpoint({this.request});

  @override
  Uri create() {
    final params = {'uuid': request.uuid};
    return Uri.http(Urls.managerUrl, Urls.getNameLike, params);
  }
}
