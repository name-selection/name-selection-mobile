import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'name_info_request.g.dart';

abstract class NameInfoRequest implements Built<NameInfoRequest, NameInfoRequestBuilder> {
  /// Уникальный идентификатор имени
  @nullable
  String get uuid;

  NameInfoRequest._();
  factory NameInfoRequest([void Function(NameInfoRequestBuilder) updates]) = _$NameInfoRequest;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(NameInfoRequest.serializer, this);
  }

  static NameInfoRequest fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(NameInfoRequest.serializer, json);
  }

  static Serializer<NameInfoRequest> get serializer => _$nameInfoRequestSerializer;
}
