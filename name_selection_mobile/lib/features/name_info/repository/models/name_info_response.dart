import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/domain/objects/models/models.dart';
import 'package:name_selection_mobile/core/network/models/base_model.dart';

import 'serializers.dart';

part 'name_info_response.g.dart';

abstract class NameInfoResponse implements BaseModel, Built<NameInfoResponse, NameInfoResponseBuilder> {
  /// Информация об имени
  @nullable
  Name get name;

  @nullable
  String get message;

  NameInfoResponse._();
  factory NameInfoResponse([void Function(NameInfoResponseBuilder) updates]) = _$NameInfoResponse;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(NameInfoResponse.serializer, this);
  }

  static NameInfoResponse fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(NameInfoResponse.serializer, json);
  }

  static Serializer<NameInfoResponse> get serializer => _$nameInfoResponseSerializer;
}
