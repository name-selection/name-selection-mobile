import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'name_like_request.g.dart';

abstract class NameLikeRequest implements Built<NameLikeRequest, NameLikeRequestBuilder> {
  /// Уникальный идентификатор имени
  @nullable
  String get uuid;

  NameLikeRequest._();
  factory NameLikeRequest([void Function(NameLikeRequestBuilder) updates]) = _$NameLikeRequest;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(NameLikeRequest.serializer, this);
  }

  static NameLikeRequest fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(NameLikeRequest.serializer, json);
  }

  static Serializer<NameLikeRequest> get serializer => _$nameLikeRequestSerializer;
}
