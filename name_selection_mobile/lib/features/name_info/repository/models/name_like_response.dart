import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/domain/objects/models/models.dart';
import 'package:name_selection_mobile/core/network/models/base_model.dart';

import 'serializers.dart';

part 'name_like_response.g.dart';

abstract class NameLikeResponse implements BaseModel, Built<NameLikeResponse, NameLikeResponseBuilder> {
  /// Количество лайков
  @nullable
  num get likes;
  
  /// Информация об имени
  @nullable
  Name get name;

  @nullable
  String get message;

  NameLikeResponse._();
  factory NameLikeResponse([void Function(NameLikeResponseBuilder) updates]) = _$NameLikeResponse;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(NameLikeResponse.serializer, this);
  }

  static NameLikeResponse fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(NameLikeResponse.serializer, json);
  }

  static Serializer<NameLikeResponse> get serializer => _$nameLikeResponseSerializer;
}
