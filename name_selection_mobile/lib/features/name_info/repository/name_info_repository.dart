import 'dart:async';
import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/utilities/isolate_manager/isolate_manager_mixin.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import 'endpoints/endpoints.dart';
import 'models/models.dart';

abstract class NameInfoRepository {
  Stream<NameInfoResponse> makeNameInfoRequest(NameInfoRequest request, {Duration timeout});
  Stream<NameLikeResponse> makeNameLikeRequest(NameLikeRequest request, {Duration timeout});
}

@Injectable(as: NameInfoRepository)
class NameInfoRepositoryImpl with IsolateManagerMixin implements NameInfoRepository {
  final RestService _restService;
  final UrlFactory _urlFactory;

  NameInfoRepositoryImpl(this._restService, this._urlFactory);

  /// Запрос для загрузки списка мужских имен
  @override
  Stream<NameInfoResponse> makeNameInfoRequest(NameInfoRequest request,
      {Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<NameInfoResponse>();
    subscribe(inputSubject, outputSubject, nameInfoMapRestBundle);
    _makeNameInfoRequest(request, input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeNameInfoRequest(NameInfoRequest request,
      {BehaviorSubject<RestBundle> input, BehaviorSubject<NameInfoResponse> output, Duration timeout}) async {
    final endpoint = NameInfoEndpoint(request: request);
    var url = _urlFactory.createFor<NameInfoEndpoint>(endpoint);
    executeRestGetStringRequest(input, output, _restService, url, NameInfoResponse.serializer, timeout);
  }

  /// Запрос для увеличения популярности имени
  @override
  Stream<NameLikeResponse> makeNameLikeRequest(NameLikeRequest request,
      {Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<NameLikeResponse>();
    subscribe(inputSubject, outputSubject, nameLikeMapRestBundle);
    _makeNameLikeRequest(request, input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeNameLikeRequest(NameLikeRequest request,
      {BehaviorSubject<RestBundle> input, BehaviorSubject<NameLikeResponse> output, Duration timeout}) async {
    final endpoint = NameLikeEndpoint(request: request);
    var url = _urlFactory.createFor<NameLikeEndpoint>(endpoint);
    final String requestString = json.encode(request);
    executeRestPostStringRequest(
        input, output, _restService, url, requestString, NameLikeResponse.serializer, timeout, '', false);
  }
}

NameInfoResponse nameInfoMapRestBundle(RestBundle bundle) {
  if (bundle.status != 200) {
    return NameInfoResponse((builder) => builder
      ..httpCode = bundle.status
      ..message = bundle.data.toString());
  }
  try {
    final jsonDecoded = {'name': jsonDecode(utf8.decode(bundle?.bodyBytes ?? 0))};
    NameInfoResponse response = serializers.deserializeWith(bundle.serializer, jsonDecoded);
    return response.rebuild((builder) => builder.httpCode = bundle.status);
  } catch (err) {
    logger.e('nameInfoMapRestBundle $err');
    return NameInfoResponse((builder) => builder.httpCode = bundle.status);
  }
}

NameLikeResponse nameLikeMapRestBundle(RestBundle bundle) {
  if (bundle.status != 200) {
    return NameLikeResponse((builder) => builder
      ..httpCode = bundle.status
      ..message = bundle.data.toString());
  }
  try {
    final jsonDecoded = jsonDecode(bundle?.data ?? '');
    NameLikeResponse response = serializers.deserializeWith(bundle.serializer, jsonDecoded);
    return response.rebuild((builder) => builder.httpCode = bundle.status);
  } catch (err) {
    logger.e('nameLikeMapRestBundle $err');
    return NameLikeResponse((builder) => builder.httpCode = bundle.status);
  }
}
