import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'name_list_screen_status.g.dart';

class NameListScreenStatus extends EnumClass {
  static const NameListScreenStatus fail = _$fail;
  static const NameListScreenStatus loading = _$loading;
  static const NameListScreenStatus waiting = _$waiting;

  const NameListScreenStatus._(String name) : super(name);

  static BuiltSet<NameListScreenStatus> get values => _$nameListScreenStatusValues;
  static NameListScreenStatus valueOf(String name) => _$nameListScreenStatusValueOf(name);
}
