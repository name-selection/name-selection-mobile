import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../../name_list.dart';

part 'name_list_actions.g.dart';

abstract class NameListActions extends ReduxActions {
  NameListActions._();

  factory NameListActions() => _$NameListActions();

  ActionDispatcher<void> maleNameListRequest;
  ActionDispatcher<void> femaleNameListRequest;
  ActionDispatcher<NameListResponse> setNameListResponse;

  ActionDispatcher<GenderTypeEnum> setGenderType;
  ActionDispatcher<BuiltList<Name>> setNameList;
  ActionDispatcher<NameListScreenStatus> setNameListScreenStatus;
}
