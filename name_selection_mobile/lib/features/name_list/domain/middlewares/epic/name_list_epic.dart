import 'dart:async';

import 'package:built_redux/built_redux.dart';
import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import '../../../name_list.dart';

@Injectable()
class NameListEpic {
  NameListEpic(this._repository);

  final NameListRepository _repository;

  Stream maleNameListEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == NameListActionsNames.maleNameListRequest.name)
        .cast<Action<void>>()
        .switchMap((action) {
      return _repository.makeMaleNameListRequest(
        timeout: Duration(seconds: 20),
      );
    }).doOnData((maleNameListResponse) {
      api.actions.nameListActions.setNameListResponse(maleNameListResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }

  Stream femaleNameListEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == NameListActionsNames.femaleNameListRequest.name)
        .cast<Action<void>>()
        .switchMap((action) {
      return _repository.makeFemaleNameListRequest(
        timeout: Duration(seconds: 20),
      );
    }).doOnData((femaleNameListResponse) {
      api.actions.nameListActions.setNameListResponse(femaleNameListResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }
}
