import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../../name_list.dart';

MiddlewareBuilder<AppState, AppStateBuilder, AppActions> nameListMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
    ..add(NameListActionsNames.maleNameListRequest, _maleNameListRequest)
    ..add(NameListActionsNames.femaleNameListRequest, _femaleNameListRequest)
    ..add(NameListActionsNames.setNameListResponse, _setNameListResponse);
}

void _maleNameListRequest(MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<void> action) {
  next(action);
  api.actions.nameListActions.setNameListScreenStatus(NameListScreenStatus.loading);
}

void _femaleNameListRequest(MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<void> action) {
  next(action);
  api.actions.nameListActions.setNameListScreenStatus(NameListScreenStatus.loading);
}

void _setNameListResponse(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<NameListResponse> action) {
  next(action);

  final response = action.payload;

  switch (response.httpCode) {
    case 200:
      api.actions.nameListActions.setNameList(response?.names ?? BuiltList<Name>());
      api.actions.nameListActions.setNameListScreenStatus(NameListScreenStatus.waiting);
      print('Успешно');
      break;
    default:
      api.actions.nameListActions.setNameListScreenStatus(NameListScreenStatus.fail);
      print('Ошибка');
  }
}
