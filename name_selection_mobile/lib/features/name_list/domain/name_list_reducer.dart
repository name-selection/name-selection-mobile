import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../name_list.dart';

NestedReducerBuilder<AppState, AppStateBuilder, NameListState, NameListStateBuilder> createNameListReducer() {
  return NestedReducerBuilder<AppState, AppStateBuilder, NameListState, NameListStateBuilder>(
    (state) => state.nameListState,
    (builder) => builder.nameListState,
  )
    ..add(NameListActionsNames.setGenderType, _setGenderType)
    ..add(NameListActionsNames.setNameList, _setNameList)
    ..add(NameListActionsNames.setNameListScreenStatus, _setNameListScreenStatus);
}

void _setGenderType(NameListState state, Action<GenderTypeEnum> action, NameListStateBuilder builder) {
  builder.genderType = action.payload;
}

void _setNameList(NameListState state, Action<BuiltList<Name>> action, NameListStateBuilder builder) {
  builder.names.replace(action.payload);
}

void _setNameListScreenStatus(NameListState state, Action<NameListScreenStatus> action, NameListStateBuilder builder) {
  builder.nameListScreenStatus = action.payload;
}
