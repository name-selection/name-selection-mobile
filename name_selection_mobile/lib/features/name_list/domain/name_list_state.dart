import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_list/data/data.dart';

part 'name_list_state.g.dart';

abstract class NameListState implements Built<NameListState, NameListStateBuilder> {
  /// Тип гендера
  @nullable
  GenderTypeEnum get genderType;

  /// Список имен
  @nullable
  BuiltList<Name> get names;

  /// Статус экрана
  @nullable
  NameListScreenStatus get nameListScreenStatus;

  NameListState._();

  factory NameListState([void Function(NameListStateBuilder) updates]) = _$NameListState;
}
