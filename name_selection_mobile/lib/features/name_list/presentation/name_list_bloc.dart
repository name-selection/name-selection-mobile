import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_info/presentation/name_info_screen.dart';

class NameListBloc extends BaseBloc {
  final GenderTypeEnum genderType;
  NameListBloc(this.genderType);

  /// Контроллер списка имен
  StreamController<BuiltList<Name>> _namesController;
  StreamSubscription<BuiltList<Name>> _namesSubscription;
  Stream<BuiltList<Name>> get namesStream => _namesController.stream;

  @override
  void init() {
    super.init();

    if (genderType == GenderTypeEnum.male) {
      store.actions.nameListActions.maleNameListRequest();
    }

    if (genderType == GenderTypeEnum.female) {
      store.actions.nameListActions.femaleNameListRequest();
    }

    _namesController = StreamController<BuiltList<Name>>.broadcast();
    _namesSubscription =
        store.nextSubstate((AppState state) => state.nameListState.names).listen((BuiltList<Name> value) {
      _namesController.sink.add(value);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _namesController.close();
    _namesSubscription.cancel();
  }

  List<Name> get namesList => store?.state?.nameListState?.names?.toList() ?? [];

  /// Переход на окно с информацией об имени
  void openNameInfo(BuildContext context, {Name name, FutureOr callback}) {
    Route route = MaterialPageRoute(builder: (context) => NameInfoScreen(name: name));
    Navigator.push(context, route).then(callback);
  }
}
