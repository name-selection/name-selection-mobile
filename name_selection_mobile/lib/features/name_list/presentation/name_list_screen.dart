import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:provider/provider.dart';

import 'name_list_bloc.dart';
import 'name_list_view.dart';

class NameListScreen extends StatelessWidget {
  final GenderTypeEnum genderType;

  const NameListScreen({
    Key key,
    @required this.genderType,
  })  : assert(genderType != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (ctx) => NameListBloc(genderType)..init(),
      dispose: (ctx, bloc) => bloc.dispose(),
      child: NameListView(genderType),
    );
  }
}
