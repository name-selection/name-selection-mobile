import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/drawer_menu/drawer_menu.dart';
import 'package:name_selection_mobile/features/name_list/presentation/presentation.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NameListView extends StatefulWidget {
  final GenderTypeEnum genderType;

  const NameListView(this.genderType);

  @override
  _NameListViewState createState() => _NameListViewState();
}

class _NameListViewState extends State<NameListView> {
  NameListBloc get bloc => Provider.of<NameListBloc>(context, listen: false);

  TextEditingController _searchQueryController = TextEditingController();
  bool _isSearching = false;
  String searchQuery = "";

  FocusNode _textFocusNode;

  /// Для отслеживания избранных имен на иконках
  Future<Set<String>> _favouriteNames;

  @override
  void initState() {
    super.initState();
    _updateData();
    _textFocusNode = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    _textFocusNode.dispose();
  }

  /// Callback при возврате
  FutureOr onGoBack(dynamic value) {
    _updateData();
    setState(() {});
  }

  /// Обновление данных
  void _updateData() {
    _favouriteNames = SharedPreferences.getInstance().then((SharedPreferences prefs) {
      return (prefs.getKeys());
    });
  }

  /// Добавление имени в избранные
  Future<void> _addFavourite(Name name) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(name.uuid, name.name);
    setState(() {
      _updateData();
    });
  }

  /// Удаление имени из избранных
  Future<void> _removeFavourite(Name name) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(name.uuid);
    setState(() {
      _updateData();
    });
  }

  /// Проверка наличия имени в избранных именах
  bool _checkFavourite(Name name, Set<String> data) {
    return data?.contains(name.uuid) ?? false;
  }

  /// Поисковая строка
  Widget _buildSearchField() {
    return TextField(
      focusNode: _textFocusNode,
      controller: _searchQueryController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: "Поиск имени",
        contentPadding: const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
        border: UnderlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(5.0),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      style: TextStyle(color: Colors.black, fontSize: 16.0),
      onChanged: (query) => updateSearchQuery(query),
    );
  }

  /// Кнопка для начала поиска
  List<Widget> _buildActions() {
    if (!_isSearching) {
      return <Widget>[
        IconButton(
          icon: const Icon(Icons.search),
          onPressed: _startSearch,
        ),
      ];
    }
    return null;
  }

  /// Начало поиска
  void _startSearch() {
    ModalRoute.of(context).addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));
    _textFocusNode.requestFocus();

    setState(() {
      _isSearching = true;
    });
  }

  /// Конец поиска
  void _stopSearching() {
    _clearSearchQuery();
    _textFocusNode.unfocus();

    setState(() {
      _isSearching = false;
    });
  }

  /// Обновление поискового запроса
  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
  }

  /// Очистка поискового запроса
  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      updateSearchQuery("");
    });
  }

  /// Скрытие клавиатуры
  void _hideKeyboard() {
    _stopSearching();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: _isSearching ? const BackButton() : null,
        actions: _buildActions(),
        title: _isSearching
            ? _buildSearchField()
            : Text('${widget.genderType == GenderTypeEnum.male ? 'Мужские имена' : 'Женские имена'}'),
      ),
      drawer: DrawerMenu(
          selectedRoute: widget.genderType == GenderTypeEnum.male ? Routes.maleNameList : Routes.femaleNameList),
      body: GestureDetector(
        onTap: () => _hideKeyboard(),
        child: StreamBuilder<BuiltList<Name>>(
          stream: bloc.namesStream,
          builder: (context, snapshot) {
            final List<Name> searchList =
                bloc.namesList.where((e) => e.name.toLowerCase().startsWith(searchQuery.toLowerCase())).toList();
            final data = bloc.namesList ?? snapshot?.data?.toList();
            if (data?.isEmpty ?? false) {
              return Container(
                child: Center(
                  child: Text(
                    'Нет данных',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              );
            } else if (data != null) {
              return FutureBuilder<Set<String>>(
                future: _favouriteNames,
                builder: (ctx, snap) {
                  return GroupedListView<Name, String>(
                    shrinkWrap: true,
                    elements: searchList ?? <Name>[],
                    groupBy: (element) => element.name.toLowerCase()[0],
                    groupSeparatorBuilder: (String value) => DividerWithText(
                      text: value.toUpperCase(),
                      textColor: Color(0xff1664A7),
                      dividerColor: Color(0xff1664A7),
                    ),
                    itemBuilder: (context, name) {
                      return ListTile(
                        title: Text(name.name),
                        visualDensity: VisualDensity.compact,
                        trailing: _checkFavourite(name, snap.data)
                            ? IconButton(
                                icon: Icon(
                                  Icons.favorite_rounded,
                                  color: Colors.red,
                                ),
                                onPressed: () => _removeFavourite(name),
                              )
                            : IconButton(
                                icon: Icon(Icons.favorite_border),
                                onPressed: () => _addFavourite(name),
                              ),
                        onTap: () => bloc.openNameInfo(context, name: name, callback: onGoBack),
                      );
                    },
                    separator: Divider(
                      color: Colors.grey,
                      indent: 10,
                      endIndent: 10,
                    ),
                    order: GroupedListOrder.ASC,
                  );
                },
              );
            } else {
              return Container(
                child: Center(
                  child: Text(
                    'Не удалось загрузить данные',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
