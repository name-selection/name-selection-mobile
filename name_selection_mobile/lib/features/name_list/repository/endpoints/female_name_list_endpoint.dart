import 'package:name_selection_mobile/core/core.dart';

class FemaleNameListEndpoint implements Endpoint {
  FemaleNameListEndpoint();

  @override
  Uri create() {
    return Uri.http(
      Urls.managerUrl,
      Urls.femaleNameList,
    );
  }
}
