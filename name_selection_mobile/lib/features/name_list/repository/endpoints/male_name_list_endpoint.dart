import 'package:name_selection_mobile/core/core.dart';

class MaleNameListEndpoint implements Endpoint {
  MaleNameListEndpoint();

  @override
  Uri create() {
    return Uri.http(
      Urls.managerUrl,
      Urls.maleNameList,
    );
  }
}
