import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/domain/objects/models/models.dart';
import 'package:name_selection_mobile/core/network/models/base_model.dart';

import 'serializers.dart';

part 'name_list_response.g.dart';

abstract class NameListResponse implements BaseModel, Built<NameListResponse, NameListResponseBuilder> {
  /// Список имен
  @nullable
  BuiltList<Name> get names;

  @nullable
  String get message;

  NameListResponse._();
  factory NameListResponse([void Function(NameListResponseBuilder) updates]) = _$NameListResponse;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(NameListResponse.serializer, this);
  }

  static NameListResponse fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(NameListResponse.serializer, json);
  }

  static Serializer<NameListResponse> get serializer => _$nameListResponseSerializer;
}
