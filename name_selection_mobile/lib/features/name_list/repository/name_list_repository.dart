import 'dart:async';
import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/utilities/isolate_manager/isolate_manager_mixin.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import 'endpoints/endpoints.dart';
import 'models/models.dart';

abstract class NameListRepository {
  Stream<NameListResponse> makeMaleNameListRequest({Duration timeout});

  Stream<NameListResponse> makeFemaleNameListRequest({Duration timeout});
}

@Injectable(as: NameListRepository)
class NameListRepositoryImpl with IsolateManagerMixin implements NameListRepository {
  final RestService _restService;
  final UrlFactory _urlFactory;

  NameListRepositoryImpl(this._restService, this._urlFactory);

  /// Запрос для загрузки списка мужских имен
  @override
  Stream<NameListResponse> makeMaleNameListRequest({Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<NameListResponse>();
    subscribe(inputSubject, outputSubject, nameListMapRestBundle);
    _makeMaleNameListRequest(input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeMaleNameListRequest(
      {BehaviorSubject<RestBundle> input, BehaviorSubject<NameListResponse> output, Duration timeout}) async {
    final endpoint = MaleNameListEndpoint();
    var url = _urlFactory.createFor<MaleNameListEndpoint>(endpoint);
    executeRestGetStringRequest(input, output, _restService, url, NameListResponse.serializer, timeout);
  }

  /// Запрос для загрузки списка женских имен
  @override
  Stream<NameListResponse> makeFemaleNameListRequest({Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<NameListResponse>();
    subscribe(inputSubject, outputSubject, nameListMapRestBundle);
    _makeFemaleNameListRequest(input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeFemaleNameListRequest(
      {BehaviorSubject<RestBundle> input, BehaviorSubject<NameListResponse> output, Duration timeout}) async {
    final endpoint = FemaleNameListEndpoint();
    var url = _urlFactory.createFor<FemaleNameListEndpoint>(endpoint);
    executeRestGetStringRequest(input, output, _restService, url, NameListResponse.serializer, timeout);
  }
}

NameListResponse nameListMapRestBundle(RestBundle bundle) {
  if (bundle.status != 200) {
    return NameListResponse((builder) => builder
      ..httpCode = bundle.status
      ..message = bundle.data.toString());
  }
  try {
    final jsonDecoded = {'names': jsonDecode(utf8.decode(bundle?.bodyBytes ?? 0))};
    NameListResponse response = serializers.deserializeWith(bundle.serializer, jsonDecoded);
    return response.rebuild((builder) => builder.httpCode = bundle.status);
  } catch (err) {
    logger.e('nameListMapRestBundle $err');
    return NameListResponse((builder) => builder.httpCode = bundle.status);
  }
}
