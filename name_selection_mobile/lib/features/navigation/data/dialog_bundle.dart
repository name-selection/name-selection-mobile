import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/features/navigation/data/dialogs.dart';

part 'dialog_bundle.g.dart';

abstract class AlertDialogBundle
    implements Built<AlertDialogBundle, AlertDialogBundleBuilder> {
  AlertDialogBundle._();

  factory AlertDialogBundle([updates(AlertDialogBundleBuilder buider)]) {
    return _$AlertDialogBundle((b) => b
      ..dialogType = DialogType.alert
      ..update(updates));
  }

  @nullable
  DialogType get dialogType;

  @nullable
  String get buttonText;

  @nullable
  String get title;

  @nullable
  String get message;

  static Serializer<AlertDialogBundle> get serializer =>
      _$alertDialogBundleSerializer;
}
