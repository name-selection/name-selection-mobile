import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/features/navigation/data/notifications.dart';

part 'notification_bundle.g.dart';

abstract class NotificationBundle
    implements Built<NotificationBundle, NotificationBundleBuilder> {
  NotificationBundle._();

  factory NotificationBundle([updates(NotificationBundleBuilder buider)]) {
    return _$NotificationBundle((b) => b..update(updates));
  }

  @nullable
  NotificationType get notificationType;

  @nullable
  String get buttonText;

  @nullable
  String get title;

  @nullable
  String get message;

  static Serializer<NotificationBundle> get serializer =>
      _$notificationBundleSerializer;
}
