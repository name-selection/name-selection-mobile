import 'dart:ui';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:flutter/material.dart';
import 'package:fluttericon/typicons_icons.dart';

part 'notifications.g.dart';

class NotificationType extends EnumClass {
  static const NotificationType alert = _$alert;
  static const NotificationType info = _$info;
  static const NotificationType error = _$error;
  static const NotificationType success = _$success;
  static const NotificationType message = _$message;

  const NotificationType._(String name) : super(name);

  static BuiltSet<NotificationType> get values => _$notificationTypeValues;

  static NotificationType valueOf(String name) =>
      _$notificationTypeValueOf(name);

  Color get color =>
      _color[this] ?? (throw StateError('No color for notificationType.$name'));

  static const _color = const {
    message: Colors.blue,
    success: Colors.lightGreen,
    info: Colors.amber,
    alert: Colors.deepOrangeAccent,
    error: Colors.red,
  };

  Icon get icon =>
      _icon[this] ?? (throw StateError('No icon for notificationType.$name'));

  static const _icon = const {
    message: Icons.mail_outline,
    success: Icons.check_circle_outline,
    info: Icons.info_outline,
    alert: Icon(Typicons.warning_empty),
    error: Icon(
      Icons.error_outline,
      color: Colors.red,
    ),
  };
}
