import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'routes.g.dart';

class Routes extends EnumClass {
  static const Routes login = _$login;
  static const Routes maleNameList = _$maleNameList;
  static const Routes femaleNameList = _$femaleNameList;
  static const Routes topList = _$topList;
  static const Routes randomName = _$randomName;
  static const Routes nameInfo = _$nameInfo;
  static const Routes favouriteNames = _$favouriteNames;
  static const Routes compatibility = _$compatibility;
  
  static const Routes pop = _$pop;

  const Routes._(String name) : super(name);

  static BuiltSet<Routes> get values => _$routesValues;

  static Routes valueOf(String name) => _$routesValueOf(name);
}
