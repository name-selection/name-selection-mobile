import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';
import 'package:name_selection_mobile/features/navigation/domain/app_route.dart';

part 'navigation_actions.g.dart';

abstract class NavigationActions extends ReduxActions {
  NavigationActions._();

  factory NavigationActions() = _$NavigationActions;

  ActionDispatcher<AppRoute> get routeTo;
  ActionDispatcher<AlertDialogBundle> get showDialog;
  ActionDispatcher<NotificationBundle> get showNotification;
}
