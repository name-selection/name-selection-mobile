import 'package:built_value/built_value.dart';
import 'package:flutter/widgets.dart' show BuildContext, NavigatorState;
import 'package:name_selection_mobile/features/navigation/data/routes.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';
import 'package:name_selection_mobile/features/navigation/data/transition_type.dart';

part 'app_route.g.dart';

abstract class AppRoute implements Built<AppRoute, AppRouteBuilder> {
  AppRoute._();

  factory AppRoute([updates(AppRouteBuilder builder)]) = _$AppRoute;

  Routes get route;

  @nullable
  String get payload;

  @nullable
  String get screenTitle;

  @nullable
  NavigationType get navigationType;

  @nullable
  TransitionType get transitionType;

  @BuiltValueField(serialize: false)
  @nullable
  BuildContext get context;

  @BuiltValueField(serialize: false)
  @nullable
  NavigatorState get navigatorState;

  @BuiltValueField(serialize: false)
  @nullable
  Object get bundle;
}
