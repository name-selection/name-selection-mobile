import 'package:built_redux/built_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';
import 'package:name_selection_mobile/features/navigation/domain/actions/navigation_actions.dart';
import 'package:name_selection_mobile/features/navigation/routes/dialog_route.dart';

//ignore_for_file: unawaited_futures

MiddlewareBuilder<AppState, AppStateBuilder, AppActions> dialogMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
    ..add(NavigationActionsNames.showDialog, _showDialog);
}

void _showDialog(MiddlewareApi<AppState, AppStateBuilder, AppActions> api,
    ActionHandler next, Action<AlertDialogBundle> action) async {
  next(action);
  final payload = action.payload;
  final rootNavigator = api.state.navigationState.rootNavigatorKey.currentState;

  switch (payload.dialogType) {
    case DialogType.alert:
      await rootNavigator.push(
        DialogRoute(
          pageBuilder: (BuildContext buildContext, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return Builder(builder: (BuildContext context) {
              return TeconTaskAlertDialog(
                buttonText: payload.buttonText,
                message: payload.message,
                title: payload.title,
              );
            });
          },
        ),
      );
      break;
    default:
      break;
  }
}
