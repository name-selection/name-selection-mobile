import 'package:built_redux/built_redux.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:name_selection_mobile/features/compatibility/presentation/compatibility_screen.dart';
import 'package:name_selection_mobile/features/favourite_list/favourite_list.dart';
import 'package:name_selection_mobile/features/name_info/name_info.dart';
import 'package:name_selection_mobile/features/name_list/name_list.dart';
import 'package:name_selection_mobile/features/navigation/data/transition_type.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/random_name/random_name.dart';
import 'package:name_selection_mobile/features/top_list/top_list.dart';

import '../../navigation.dart';
import '../actions/navigation_actions.dart';
import '../app_route.dart';

MiddlewareBuilder<AppState, AppStateBuilder, AppActions> navigationMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()..add(NavigationActionsNames.routeTo, routeTo);
}

void routeTo(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, ActionHandler next, Action<AppRoute> action) async {
  next(action);
  final payload = action.payload;
  final rootNavigator = api.state.navigationState.rootNavigatorKey.currentState;
  dynamic materialPageRoute;

  RouteSettings _settings = RouteSettings(name: payload.route.name);
  Widget _nextPage;
  switch (payload.route) {

    /// TODO: авторизация
    // case Routes.login:
    //   _nextPage = LoginScreen();
    //   break;
    case Routes.maleNameList:
      final GenderTypeEnum genderType = GenderTypeEnum.male;
      assert(genderType != null);
      _nextPage = NameListScreen(genderType: genderType);
      break;
    case Routes.femaleNameList:
      final GenderTypeEnum genderType = GenderTypeEnum.female;
      assert(genderType != null);
      _nextPage = NameListScreen(genderType: genderType);
      break;
    case Routes.topList:
      _nextPage = TopListScreen();
      break;
    case Routes.randomName:
      _nextPage = RandomNameScreen();
      break;
    case Routes.favouriteNames:
      _nextPage = FavouriteListScreen();
      break;
    case Routes.compatibility:
      _nextPage = CompatibilityScreen();
      break;
    case Routes.nameInfo:
      final Name name = payload.bundle;
      assert(name != null);
      _nextPage = NameInfoScreen(name: name);
      break;
    case Routes.pop:
      rootNavigator.maybePop().then((value) {
        if (payload.bundle != null) {
          final message = (payload.bundle as Map<String, Object>)['message'];
          final icon = (payload.bundle as Map<String, Object>)['icon'];
          final iconWidget = icon != null ? Icon(icon, color: Colors.white) : null;
          ShowSnackBar.showInfoSnackBar(message: message, icon: iconWidget); //showSnackBar
        } // (payload.bundle);
      });
      break;
    default:
      break;
  }
  switch (payload.transitionType) {
    case TransitionType.backSlide:
      {
        ///Пока лучше не использовать этот тип.
        materialPageRoute = BackSlideRoute(settings: _settings, enterPage: _nextPage, exitPage: null);
        break;
      }
    case TransitionType.fade:
      {
        materialPageRoute = FadeRoute(settings: _settings, page: _nextPage);
        break;
      }
    case TransitionType.rightSlide:
      {
        materialPageRoute = RightSlideRoute(settings: _settings, page: _nextPage);
        break;
      }
    case TransitionType.scale:
      {
        materialPageRoute = ScaleRoute(settings: _settings, page: _nextPage);
        break;
      }
    case TransitionType.size:
      {
        materialPageRoute = SizeRoute(settings: _settings, page: _nextPage);
        break;
      }
    default:
      materialPageRoute = MaterialPageRoute(
        settings: _settings,
        builder: (context) {
          return _nextPage;
        },
      );
      break;
  }

  if (materialPageRoute != null) {
    final NavigationType navigationType = payload.navigationType ?? NavigationType.push;
    switch (navigationType) {
      case NavigationType.push:
        rootNavigator.push(materialPageRoute);
        break;
      case NavigationType.pushReplacement:
        rootNavigator.pushReplacement(materialPageRoute);
        break;
      case NavigationType.pushAndRemoveUntil:
        rootNavigator.pushAndRemoveUntil(materialPageRoute, (Route<dynamic> route) => false);
        break;
    }
  }
}
