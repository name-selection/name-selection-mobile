import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';

import '../../navigation.dart';

MiddlewareBuilder<AppState, AppStateBuilder, AppActions>
    notificationMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
    ..add(NavigationActionsNames.showNotification, _showNotification);
}

void _showNotification(MiddlewareApi<AppState, AppStateBuilder, AppActions> api,
    ActionHandler next, Action<NotificationBundle> action) async {
  next(action);
  final payload = action.payload;

  switch (payload.notificationType) {
    // TODO: реализовать
    // case NotificationType.error:
    //   ShowSnackBar.showWarningSnackBar(
    //     message: payload.message,
    //   );
    //   break;
    default:
      break;
  }
}
