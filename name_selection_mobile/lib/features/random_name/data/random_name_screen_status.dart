import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'random_name_screen_status.g.dart';

class RandomNameScreenStatus extends EnumClass {
  static const RandomNameScreenStatus fail = _$fail;
  static const RandomNameScreenStatus loading = _$loading;
  static const RandomNameScreenStatus waiting = _$waiting;

  const RandomNameScreenStatus._(String name) : super(name);

  static BuiltSet<RandomNameScreenStatus> get values => _$randomNameScreenStatusValues;
  static RandomNameScreenStatus valueOf(String name) => _$randomNameScreenStatusValueOf(name);
}
