import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../../random_name.dart';

part 'random_name_actions.g.dart';

abstract class RandomNameActions extends ReduxActions {
  RandomNameActions._();

  factory RandomNameActions() => _$RandomNameActions();

  ActionDispatcher<RandomNameRequest> randomNameRequest;
  ActionDispatcher<RandomNameResponse> setRandomNameResponse;

  ActionDispatcher<GenderTypeEnum> setGenderType;
  ActionDispatcher<RandomNameScreenStatus> setRandomNameScreenStatus;
}
