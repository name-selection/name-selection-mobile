import 'dart:async';

import 'package:built_redux/built_redux.dart';
import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import '../../../random_name.dart';

@Injectable()
class RandomNameEpic {
  RandomNameEpic(this._repository);

  final RandomNameRepository _repository;

  Stream randomNameEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == RandomNameActionsNames.randomNameRequest.name)
        .cast<Action<RandomNameRequest>>()
        .switchMap((action) {
      final RandomNameRequest request = action.payload;
      return _repository.makeRandomNameRequest(
        request,
        timeout: Duration(seconds: 20),
      );
    }).doOnData((randomNameResponse) {
      api.actions.randomNameActions.setRandomNameResponse(randomNameResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }
}
