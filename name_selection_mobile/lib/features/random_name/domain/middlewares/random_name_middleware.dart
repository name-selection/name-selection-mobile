import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';
import 'package:name_selection_mobile/features/navigation/domain/app_route.dart';

import '../../random_name.dart';

MiddlewareBuilder<AppState, AppStateBuilder, AppActions> randomNameMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
    ..add(RandomNameActionsNames.randomNameRequest, _randomNameRequest)
    ..add(RandomNameActionsNames.setRandomNameResponse, _setRandomNameResponse);
}

void _randomNameRequest(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<RandomNameRequest> action) {
  next(action);
  api.actions.randomNameActions.setRandomNameScreenStatus(RandomNameScreenStatus.loading);
}

void _setRandomNameResponse(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<RandomNameResponse> action) {
  next(action);

  final response = action.payload;

  switch (response.httpCode) {
    case 200:
      api.actions.navigationActions.routeTo(AppRoute((b) => b
        ..route = Routes.nameInfo
        ..bundle = action.payload.name
        ..navigationType = NavigationType.push
        ..transitionType = TransitionType.rightSlide));
      print('Успешно');
      api.actions.randomNameActions.setRandomNameScreenStatus(RandomNameScreenStatus.waiting);
      break;
    default:
      api.actions.randomNameActions.setRandomNameScreenStatus(RandomNameScreenStatus.fail);
      print('Ошибка');
  }
}
