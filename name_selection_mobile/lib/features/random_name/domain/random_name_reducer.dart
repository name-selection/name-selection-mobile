import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../random_name.dart';

NestedReducerBuilder<AppState, AppStateBuilder, RandomNameState, RandomNameStateBuilder> createRandomNameReducer() {
  return NestedReducerBuilder<AppState, AppStateBuilder, RandomNameState, RandomNameStateBuilder>(
    (state) => state.randomNameState,
    (builder) => builder.randomNameState,
  )
    ..add(RandomNameActionsNames.setGenderType, _setGenderType)
    ..add(RandomNameActionsNames.setRandomNameScreenStatus, _setRandomNameScreenStatus);
}

void _setGenderType(RandomNameState state, Action<GenderTypeEnum> action, RandomNameStateBuilder builder) {
  builder.genderType = action.payload;
}

void _setRandomNameScreenStatus(RandomNameState state, Action<RandomNameScreenStatus> action, RandomNameStateBuilder builder) {
  builder.randomNameScreenStatus = action.payload;
}
