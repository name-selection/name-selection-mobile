import 'package:built_value/built_value.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../random_name.dart';

part 'random_name_state.g.dart';

abstract class RandomNameState implements Built<RandomNameState, RandomNameStateBuilder> {
  /// Тип гендера
  @nullable
  GenderTypeEnum get genderType;

  /// Статус экрана
  @nullable
  RandomNameScreenStatus get randomNameScreenStatus;

  RandomNameState._();

  factory RandomNameState([void Function(RandomNameStateBuilder) updates]) = _$RandomNameState;
}
