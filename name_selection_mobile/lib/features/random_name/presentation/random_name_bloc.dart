import 'dart:async';

import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/random_name/random_name.dart';

class RandomNameBloc extends BaseBloc {
  @override
  void init() {
    super.init();
    store.actions.randomNameActions.setGenderType(GenderTypeEnum.male);

    _randomNameScreenStatusController = StreamController<RandomNameScreenStatus>.broadcast();
    _randomNameScreenStatusSubscription = store
        .nextSubstate((AppState state) => state.randomNameState.randomNameScreenStatus)
        .listen((RandomNameScreenStatus value) {
      _randomNameScreenStatusController.sink.add(value);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _randomNameScreenStatusController.close();
    _randomNameScreenStatusSubscription.cancel();
  }

  /// Контроллер статуса экрана
  StreamController<RandomNameScreenStatus> _randomNameScreenStatusController;
  StreamSubscription<RandomNameScreenStatus> _randomNameScreenStatusSubscription;
  Stream<RandomNameScreenStatus> get randomNameScreenStatusStream => _randomNameScreenStatusController.stream;

  GenderTypeEnum get genderType => store.state.randomNameState.genderType;

  /// Изменить пол
  void changeGenderType(GenderTypeEnum gender) {
    store.actions.randomNameActions.setGenderType(gender);
  }

  /// Запрос на получение случайного имени
  void getRandomName() {
    final request = RandomNameRequest((b) => b..gender = genderType.ids);
    store.actions.randomNameActions.randomNameRequest(request);
  }
}
