import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'random_name_bloc.dart';
import 'random_name_view.dart';

class RandomNameScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (ctx) => RandomNameBloc()..init(),
      dispose: (ctx, bloc) => bloc.dispose(),
      child: RandomNameView(),
    );
  }
}
