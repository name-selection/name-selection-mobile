import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:name_selection_mobile/features/drawer_menu/drawer_menu.dart';
import 'package:name_selection_mobile/features/navigation/data/routes.dart';
import 'package:provider/provider.dart';

import '../random_name.dart';
import 'random_name_bloc.dart';
import 'widgets/widgets.dart';

class RandomNameView extends StatefulWidget {
  @override
  _RandomNameViewState createState() => _RandomNameViewState();
}

class _RandomNameViewState extends State<RandomNameView> {
  RandomNameBloc get bloc => Provider.of<RandomNameBloc>(context, listen: false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: AutoSizeText(
          'Случайное имя',
          maxLines: 1,
        ),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      drawer: DrawerMenu(
        selectedRoute: Routes.randomName,
      ),
      body: StreamBuilder<RandomNameScreenStatus>(
          stream: bloc.randomNameScreenStatusStream,
          initialData: RandomNameScreenStatus.waiting,
          builder: (context, snapshot) {
            switch (snapshot.data) {
              case RandomNameScreenStatus.loading:
                return Center(
                  child: CircularProgressIndicator(),
                );
                break;
              case RandomNameScreenStatus.fail:
                return Container(
                  child: Center(
                    child: Text(
                      'Не удалось загрузить данные',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                );
              case RandomNameScreenStatus.waiting:
              default:
                return SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      RandomNameTitle(
                        'Настройки подбора имени',
                        fontSize: 24,
                      ),
                      RandomNameGenderType(),

                      /// TODO: поменять при расширении бизнес-логики
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15.0),
                        child: FlatButton(
                          colorBrightness: Brightness.dark,
                          color: Theme.of(context).backgroundColor,
                          onPressed: () => bloc.getRandomName(),
                          child: Text(
                            'Получить имя',
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                );
                break;
            }
          }),
    );
  }
}
