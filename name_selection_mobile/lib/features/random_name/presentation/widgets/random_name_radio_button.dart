import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:provider/provider.dart';

import '../random_name_bloc.dart';

class RandomNameGenderType extends StatefulWidget {
  RandomNameGenderType();

  @override
  _RandomNameGenderTypeState createState() => _RandomNameGenderTypeState();
}

class _RandomNameGenderTypeState extends State<RandomNameGenderType> {
  RandomNameBloc get bloc => Provider.of<RandomNameBloc>(context, listen: false);
  GenderTypeEnum _gender = GenderTypeEnum.male;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 5.0, 22.0, 0.0),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.85,
        decoration: BoxDecoration(
          color: Color(0xFFFFF4FF),
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
        child: Column(
          children: <Widget>[
            Container(
              height: 50,
              child: ListTile(
                title: Text(GenderTypeEnum.male.translates),
                leading: Radio<GenderTypeEnum>(
                  value: GenderTypeEnum.male,
                  groupValue: _gender,
                  onChanged: (GenderTypeEnum value) {
                    setState(() {
                      _gender = value;
                      bloc.changeGenderType(value);
                    });
                  },
                ),
              ),
            ),
            Container(
              height: 50,
              child: ListTile(
                title: Text(GenderTypeEnum.female.translates),
                leading: Radio<GenderTypeEnum>(
                  value: GenderTypeEnum.female,
                  groupValue: _gender,
                  onChanged: (GenderTypeEnum value) {
                    setState(() {
                      _gender = value;
                      bloc.changeGenderType(value);
                    });
                  },
                ),
              ),
            ),
            Container(
              height: 50,
              child: ListTile(
                title: Text(GenderTypeEnum.any.translates),
                leading: Radio<GenderTypeEnum>(
                  value: GenderTypeEnum.any,
                  groupValue: _gender,
                  onChanged: (GenderTypeEnum value) {
                    setState(() {
                      _gender = value;
                      bloc.changeGenderType(value);
                    });
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
