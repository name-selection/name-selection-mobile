import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class RandomNameTitle extends StatelessWidget {
  final String title;
  final double fontSize;
  RandomNameTitle(this.title, {this.fontSize = 36});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Center(
        child: AutoSizeText(
          this.title,
          maxLines: 1,
          style: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.w500,
            color: Theme.of(context).backgroundColor,
          ),
        ),
      ),
    );
  }
}
