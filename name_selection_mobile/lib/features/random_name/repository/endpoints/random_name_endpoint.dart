import 'package:name_selection_mobile/core/core.dart';
import '../repository.dart';

class RandomNameEndpoint implements Endpoint {
  final RandomNameRequest request;
  RandomNameEndpoint({this.request});

  @override
  Uri create() {
    final params = {'gender': request.gender};
    return Uri.http(Urls.managerUrl, Urls.randomName, params);
  }
}
