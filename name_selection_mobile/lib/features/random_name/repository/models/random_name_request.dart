import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';

part 'random_name_request.g.dart';

abstract class RandomNameRequest implements Built<RandomNameRequest, RandomNameRequestBuilder> {
  /// Пол
  @nullable
  String get gender;

  RandomNameRequest._();
  factory RandomNameRequest([void Function(RandomNameRequestBuilder) updates]) = _$RandomNameRequest;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(RandomNameRequest.serializer, this);
  }

  static RandomNameRequest fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(RandomNameRequest.serializer, json);
  }

  static Serializer<RandomNameRequest> get serializer => _$randomNameRequestSerializer;
}
