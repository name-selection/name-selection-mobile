import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/domain/objects/models/models.dart';
import 'package:name_selection_mobile/core/network/models/base_model.dart';

import 'serializers.dart';

part 'random_name_response.g.dart';

abstract class RandomNameResponse implements BaseModel, Built<RandomNameResponse, RandomNameResponseBuilder> {
  /// Случайное имя
  @nullable
  Name get name;

  @nullable
  String get message;

  RandomNameResponse._();
  factory RandomNameResponse([void Function(RandomNameResponseBuilder) updates]) = _$RandomNameResponse;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(RandomNameResponse.serializer, this);
  }

  static RandomNameResponse fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(RandomNameResponse.serializer, json);
  }

  static Serializer<RandomNameResponse> get serializer => _$randomNameResponseSerializer;
}
