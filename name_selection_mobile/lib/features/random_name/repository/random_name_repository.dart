import 'dart:async';
import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/utilities/isolate_manager/isolate_manager_mixin.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import 'endpoints/endpoints.dart';
import 'models/models.dart';

abstract class RandomNameRepository {
  Stream<RandomNameResponse> makeRandomNameRequest(RandomNameRequest request, {Duration timeout});
}

@Injectable(as: RandomNameRepository)
class RandomNameRepositoryImpl with IsolateManagerMixin implements RandomNameRepository {
  final RestService _restService;
  final UrlFactory _urlFactory;

  RandomNameRepositoryImpl(this._restService, this._urlFactory);

  /// Запрос для получения случайного имени
  @override
  Stream<RandomNameResponse> makeRandomNameRequest(RandomNameRequest request,
      {Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<RandomNameResponse>();
    subscribe(inputSubject, outputSubject, randomNameMapRestBundle);
    _makeRandomNameRequest(request, input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeRandomNameRequest(RandomNameRequest request,
      {BehaviorSubject<RestBundle> input, BehaviorSubject<RandomNameResponse> output, Duration timeout}) async {
    final endpoint = RandomNameEndpoint(request: request);
    var url = _urlFactory.createFor<RandomNameEndpoint>(endpoint);
    executeRestGetStringRequest(input, output, _restService, url, RandomNameResponse.serializer, timeout);
  }
}

RandomNameResponse randomNameMapRestBundle(RestBundle bundle) {
  if (bundle.status != 200) {
    return RandomNameResponse((builder) => builder
      ..httpCode = bundle.status
      ..message = bundle.data.toString());
  }
  try {
    final jsonDecoded = {'name': jsonDecode(utf8.decode(bundle?.bodyBytes ?? 0))};
    RandomNameResponse response = serializers.deserializeWith(bundle.serializer, jsonDecoded);
    return response.rebuild((builder) => builder.httpCode = bundle.status);
  } catch (err) {
    logger.e('randomNameMapRestBundle $err');
    return RandomNameResponse((builder) => builder.httpCode = bundle.status);
  }
}
