import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'top_list_screen_status.g.dart';

class TopListScreenStatus extends EnumClass {
  static const TopListScreenStatus fail = _$fail;
  static const TopListScreenStatus loading = _$loading;
  static const TopListScreenStatus waiting = _$waiting;

  const TopListScreenStatus._(String name) : super(name);

  static BuiltSet<TopListScreenStatus> get values => _$topListScreenStatusValues;
  static TopListScreenStatus valueOf(String name) => _$topListScreenStatusValueOf(name);
}
