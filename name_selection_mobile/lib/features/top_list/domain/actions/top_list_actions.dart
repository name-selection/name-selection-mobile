import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../../top_list.dart';

part 'top_list_actions.g.dart';

abstract class TopListActions extends ReduxActions {
  TopListActions._();

  factory TopListActions() => _$TopListActions();

  ActionDispatcher<void> topListRequest;
  ActionDispatcher<TopListResponse> setTopListResponse;

  ActionDispatcher<BuiltList<Name>> setTopList;
  ActionDispatcher<bool> setListOrder;
  ActionDispatcher<TopListScreenStatus> setTopListScreenStatus;
}
