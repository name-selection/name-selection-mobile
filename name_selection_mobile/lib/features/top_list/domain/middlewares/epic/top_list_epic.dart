import 'dart:async';

import 'package:built_redux/built_redux.dart';
import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import '../../../top_list.dart';

@Injectable()
class TopListEpic {
  TopListEpic(this._repository);

  final TopListRepository _repository;

  Stream topListEpic(Stream<Action<dynamic>> stream, MiddlewareApi<AppState, AppStateBuilder, AppActions> api) {
    return stream
        .where((action) => action.name == TopListActionsNames.topListRequest.name)
        .cast<Action<void>>()
        .switchMap((action) {
      return _repository.makeTopListRequest(
        timeout: Duration(seconds: 20),
      );
    }).doOnData((topListResponse) {
      api.actions.topListActions.setTopListResponse(topListResponse);
    }).handleError((exception) {
      logger.e(exception.toString());
      // api.actions.navigation.showDialog(
      //   AlertDialogBundle(
      //     (builder) => builder
      //       ..buttonText = "Ok"
      //       ..title = "Ошибка"
      //       ..message = exception.toString(),
      //   ),
      // );
    });
  }
}
