import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../../top_list.dart';

MiddlewareBuilder<AppState, AppStateBuilder, AppActions> topListMiddleware() {
  return MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
    ..add(TopListActionsNames.topListRequest, _topListRequest)
    ..add(TopListActionsNames.setTopListResponse, _setTopListResponse);
}

void _topListRequest(MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<void> action) {
  next(action);
  api.actions.topListActions.setTopListScreenStatus(TopListScreenStatus.loading);
}

void _setTopListResponse(
    MiddlewareApi<AppState, AppStateBuilder, AppActions> api, next, Action<TopListResponse> action) {
  next(action);

  final response = action.payload;

  switch (response.httpCode) {
    case 200:
      api.actions.topListActions.setTopList(response?.names ?? BuiltList<Name>());
      api.actions.topListActions.setTopListScreenStatus(TopListScreenStatus.waiting);
      print('Успешно');
      break;
    default:
      api.actions.topListActions.setTopListScreenStatus(TopListScreenStatus.fail);
      print('Ошибка');
  }
}
