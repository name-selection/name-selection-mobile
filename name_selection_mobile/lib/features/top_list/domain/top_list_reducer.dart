import 'package:built_collection/built_collection.dart';
import 'package:built_redux/built_redux.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../top_list.dart';

NestedReducerBuilder<AppState, AppStateBuilder, TopListState, TopListStateBuilder> createTopListReducer() {
  return NestedReducerBuilder<AppState, AppStateBuilder, TopListState, TopListStateBuilder>(
    (state) => state.topListState,
    (builder) => builder.topListState,
  )
    ..add(TopListActionsNames.setTopList, _setTopList)
    ..add(TopListActionsNames.setListOrder, _setListOrder)
    ..add(TopListActionsNames.setTopListScreenStatus, _setTopListScreenStatus);
}

void _setTopList(TopListState state, Action<BuiltList<Name>> action, TopListStateBuilder builder) {
  builder.names.replace(action.payload);
}

void _setListOrder(TopListState state, Action<bool> action, TopListStateBuilder builder) {
  builder.isAscending = action.payload;
}

void _setTopListScreenStatus(TopListState state, Action<TopListScreenStatus> action, TopListStateBuilder builder) {
  builder.topListScreenStatus = action.payload;
}
