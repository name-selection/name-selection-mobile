import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:name_selection_mobile/core/core.dart';

import '../top_list.dart';

part 'top_list_state.g.dart';

abstract class TopListState implements Built<TopListState, TopListStateBuilder> {
  /// Список имен
  @nullable
  BuiltList<Name> get names;

  /// Если список имен по возрастанию, то True
  @nullable
  bool get isAscending;

  /// Статус экрана
  @nullable
  TopListScreenStatus get topListScreenStatus;

  TopListState._();

  factory TopListState([void Function(TopListStateBuilder) updates]) = _$TopListState;
}
