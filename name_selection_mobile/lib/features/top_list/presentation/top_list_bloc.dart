import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/name_info/presentation/name_info_screen.dart';

class TopListBloc extends BaseBloc {
  /// Контроллер списка популярных имен
  StreamController<BuiltList<Name>> _topNamesController;
  StreamSubscription<BuiltList<Name>> _topNamesSubscription;
  Stream<BuiltList<Name>> get topNamesStream => _topNamesController.stream;

  /// Контроллер для порядка имен
  StreamController<bool> _orderListController;
  StreamSubscription<bool> _orderListSubscription;
  Stream<bool> get orderListStream => _orderListController.stream;

  /// TODO: #18 удалить заглушки
  // List<Name> listNames;

  @override
  void init() {
    super.init();

    _topNamesController = StreamController<BuiltList<Name>>.broadcast();
    _topNamesSubscription =
        store.nextSubstate((AppState state) => state.topListState.names).listen((BuiltList<Name> value) {
      _topNamesController.sink.add(value);
    });

    _orderListController = StreamController<bool>.broadcast();
    _orderListSubscription =
        store.nextSubstate((AppState state) => state.topListState.isAscending).listen((bool value) {
      _orderListController.sink.add(value);
    });

    store.actions.topListActions.topListRequest();

    // var _tempDataList = [
    //   'Алексей',
    //   'Алиса',
    //   'Анастасия',
    //   'Арина',
    //   'Артём',
    //   'Билал',
    //   'Валентина',
    //   'Валерия',
    //   'Вероника',
    //   'Виктор',
    //   'Владимир',
    //   'Владислав',
    //   'Глеб',
    //   'Даниил',
    //   'Дарья',
    //   'Екатерина',
    //   'Елизавета',
    //   'Есения',
    //   'Кира',
    //   'Кирилл',
    //   'Константин',
    //   'Лев',
    //   'Майя',
    //   'Нина',
    //   'Павел',
    //   'Теона',
    //   'Фёдор',
    //   'Элина',
    //   'Юлия',
    //   'Ясмина',
    // ];
    // listNames = <Name>[];
    // for (int i = 0; i < _tempDataList.length; i++) {
    //   listNames.add(Name((builder) => builder
    //     ..uuid = '${i * 10}'
    //     ..name = _tempDataList[i]
    //     ..description = 'Описание $i'
    //     ..gender = '${i % 2 == 0 ? 'MALE' : 'FEMALE'}'
    //     ..likes = i * 2));
    // }

    // store.actions.topListActions.setTopList(listNames.toBuiltList());
  }

  @override
  void dispose() {
    super.dispose();
    _topNamesController.close();
    _topNamesSubscription.cancel();

    _orderListController.close();
    _orderListSubscription.cancel();
  }

  List<Name> get topList => store?.state?.topListState?.names?.toList() ?? [];

  bool get isAscending => store?.state?.topListState?.isAscending ?? false;

  /// Изменить порядок списка имен
  void changeOrderList(bool value) {
    store.actions.topListActions.setListOrder(value);
  }

  /// Переход на окно с информацией об имени
  void openNameInfo(BuildContext context, {Name name, FutureOr callback}) {
    Route route = MaterialPageRoute(builder: (context) => NameInfoScreen(name: name));
    Navigator.push(context, route).then(callback);
  }
}
