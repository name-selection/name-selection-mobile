import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'top_list_bloc.dart';
import 'top_list_view.dart';

class TopListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (ctx) => TopListBloc()..init(),
      dispose: (ctx, bloc) => bloc.dispose(),
      child: TopListView(),
    );
  }
}
