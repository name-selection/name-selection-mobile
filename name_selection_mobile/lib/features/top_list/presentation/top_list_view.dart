import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/features/drawer_menu/drawer_menu.dart';
import 'package:name_selection_mobile/features/top_list/presentation/presentation.dart';
import 'package:name_selection_mobile/features/navigation/data/data.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TopListView extends StatefulWidget {
  @override
  _TopListViewState createState() => _TopListViewState();
}

class _TopListViewState extends State<TopListView> {
  TopListBloc get bloc => Provider.of<TopListBloc>(context, listen: false);

  TextEditingController _searchQueryController = TextEditingController();
  bool _isSearching = false;
  String searchQuery = "";

  FocusNode _textFocusNode;

  /// Для отслеживания избранных имен на иконках
  Future<Set<String>> _favouriteNames;

  @override
  void initState() {
    super.initState();
    _updateData();
    _textFocusNode = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    _textFocusNode.dispose();
  }

  /// Callback при возврате
  FutureOr onGoBack(dynamic value) {
    _updateData();
    setState(() {});
  }

  /// Обновление данных
  void _updateData() {
    _favouriteNames = SharedPreferences.getInstance().then((SharedPreferences prefs) {
      return (prefs.getKeys());
    });
  }

  /// Добавление имени в избранные
  Future<void> _addFavourite(Name name) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(name.uuid, name.name);
    setState(() {
      _updateData();
    });
  }

  /// Удаление имени из избранных
  Future<void> _removeFavourite(Name name) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(name.uuid);
    setState(() {
      _updateData();
    });
  }

  /// Проверка наличия имени в избранных именах
  bool _checkFavourite(Name name, Set<String> data) {
    return data?.contains(name.uuid) ?? false;
  }

  /// Поисковая строка
  Widget _buildSearchField() {
    return TextField(
      focusNode: _textFocusNode,
      controller: _searchQueryController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: "Поиск имени",
        contentPadding: const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
        border: UnderlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(5.0),
        ),
        filled: true,
        fillColor: Colors.white,
      ),
      style: TextStyle(color: Colors.black, fontSize: 16.0),
      onChanged: (query) => updateSearchQuery(query),
    );
  }

  /// Кнопка для начала поиска
  List<Widget> _buildActions() {
    if (!_isSearching) {
      return <Widget>[
        IconButton(
          icon: const Icon(Icons.search),
          onPressed: _startSearch,
        ),
      ];
    }
    return null;
  }

  /// Начало поиска
  void _startSearch() {
    ModalRoute.of(context).addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));
    _textFocusNode.requestFocus();

    setState(() {
      _isSearching = true;
    });
  }

  /// Конец поиска
  void _stopSearching() {
    _clearSearchQuery();
    _textFocusNode.unfocus();

    setState(() {
      _isSearching = false;
    });
  }

  /// Обновление поискового запроса
  void updateSearchQuery(String newQuery) {
    setState(() {
      searchQuery = newQuery;
    });
  }

  /// Очистка поискового запроса
  void _clearSearchQuery() {
    setState(() {
      _searchQueryController.clear();
      updateSearchQuery("");
    });
  }

  /// Скрытие клавиатуры
  void _hideKeyboard() {
    _stopSearching();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: _isSearching ? const BackButton() : null,
        actions: _buildActions(),
        title: _isSearching ? _buildSearchField() : Text('Популярные имена'),
      ),
      drawer: DrawerMenu(selectedRoute: Routes.topList),
      floatingActionButton: StreamBuilder<bool>(
          stream: bloc.orderListStream,
          builder: (context, snapshot) {
            return FloatingActionButton(
              child: bloc.isAscending ? Icon(Icons.arrow_upward) : Icon(Icons.arrow_downward),
              onPressed: () => bloc.changeOrderList(!bloc.isAscending),
            );
          }),
      body: GestureDetector(
        onTap: () => _hideKeyboard(),
        child: StreamBuilder<bool>(
            stream: bloc.orderListStream,
            builder: (ctx, snaphotBool) {
              return StreamBuilder<BuiltList<Name>>(
                stream: bloc.topNamesStream,
                builder: (context, snapshot) {
                  List<Name> searchList = bloc.isAscending
                      ? bloc.topList.where((e) => e.name.toLowerCase().startsWith(searchQuery.toLowerCase())).toList()
                      : bloc.topList
                          .where((e) => e.name.toLowerCase().startsWith(searchQuery.toLowerCase()))
                          .toList()
                          .reversed
                          .toList();
                  final data = bloc.topList ?? snapshot?.data?.toList();
                  if (data?.isEmpty ?? false) {
                    return Container(
                      child: Center(
                        child: Text(
                          'Нет данных',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    );
                  } else if (data != null) {
                    return FutureBuilder<Set<String>>(
                      future: _favouriteNames,
                      builder: (ctx, snap) {
                        return ListView.separated(
                          itemCount: searchList.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(searchList[index].name),
                              subtitle: Text(
                                '${searchList[index].gender == GenderTypeEnum.male.ids ? 'Мужское' : 'Женское'}',
                                style: TextStyle(
                                  color: Color.fromRGBO(122, 122, 122, 0.6),
                                ),
                              ),
                              visualDensity: VisualDensity.compact,
                              trailing: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(right: 5.0),
                                    child: Icon(
                                      Icons.remove_red_eye_outlined,
                                      color: Color.fromRGBO(122, 122, 122, 0.4),
                                    ),
                                  ),
                                  AutoSizeText(
                                    searchList[index].likes.toString(),
                                    minFontSize: 14.0,
                                    maxFontSize: 18.0,
                                    style: TextStyle(
                                      color: Color.fromRGBO(122, 122, 122, 0.6),
                                    ),
                                  ),
                                  _checkFavourite(searchList[index], snap.data)
                                      ? IconButton(
                                          icon: Icon(
                                            Icons.favorite_rounded,
                                            color: Colors.red,
                                          ),
                                          onPressed: () => _removeFavourite(searchList[index]),
                                        )
                                      : IconButton(
                                          icon: Icon(Icons.favorite_border),
                                          onPressed: () => _addFavourite(searchList[index]),
                                        ),
                                ],
                              ),
                              onTap: () => bloc.openNameInfo(context, name: searchList[index], callback: onGoBack),
                            );
                          },
                          separatorBuilder: (ctx, index) {
                            return Divider(
                              color: Colors.grey,
                              indent: 10,
                              endIndent: 10,
                            );
                          },
                        );
                      },
                    );
                  } else {
                    return Container(
                      child: Center(
                        child: Text(
                          'Не удалось загрузить данные',
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    );
                  }
                },
              );
            }),
      ),
    );
  }
}
