import 'package:name_selection_mobile/core/core.dart';

class TopListEndpoint implements Endpoint {
  TopListEndpoint();

  @override
  Uri create() {
    return Uri.http(
      Urls.managerUrl,
      Urls.topList,
    );
  }
}
