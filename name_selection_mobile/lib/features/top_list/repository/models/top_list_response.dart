import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:name_selection_mobile/core/domain/objects/models/models.dart';
import 'package:name_selection_mobile/core/network/models/base_model.dart';

import 'serializers.dart';

part 'top_list_response.g.dart';

abstract class TopListResponse implements BaseModel, Built<TopListResponse, TopListResponseBuilder> {
  /// Список имен
  @nullable
  BuiltList<Name> get names;

  @nullable
  String get message;

  TopListResponse._();
  factory TopListResponse([void Function(TopListResponseBuilder) updates]) = _$TopListResponse;

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(TopListResponse.serializer, this);
  }

  static TopListResponse fromJson(Map<String, dynamic> json) {
    return serializers.deserializeWith(TopListResponse.serializer, json);
  }

  static Serializer<TopListResponse> get serializer => _$topListResponseSerializer;
}
