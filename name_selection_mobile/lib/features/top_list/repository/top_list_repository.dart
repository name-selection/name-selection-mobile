import 'dart:async';
import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/utilities/isolate_manager/isolate_manager_mixin.dart';
import 'package:name_selection_mobile/features/logger/logger.dart';
import 'package:rxdart/rxdart.dart';

import 'endpoints/endpoints.dart';
import 'models/models.dart';

abstract class TopListRepository {
  Stream<TopListResponse> makeTopListRequest({Duration timeout});
}

@Injectable(as: TopListRepository)
class TopListRepositoryImpl with IsolateManagerMixin implements TopListRepository {
  final RestService _restService;
  final UrlFactory _urlFactory;

  TopListRepositoryImpl(this._restService, this._urlFactory);

  /// Запрос для загрузки списка популярных имен
  @override
  Stream<TopListResponse> makeTopListRequest({Duration timeout = const Duration(seconds: 20)}) {
    final inputSubject = BehaviorSubject<RestBundle>();
    final outputSubject = BehaviorSubject<TopListResponse>();
    subscribe(inputSubject, outputSubject, topListMapRestBundle);
    _makeTopListRequest(input: inputSubject, output: outputSubject, timeout: timeout);
    return outputSubject;
  }

  void _makeTopListRequest(
      {BehaviorSubject<RestBundle> input, BehaviorSubject<TopListResponse> output, Duration timeout}) async {
    final endpoint = TopListEndpoint();
    var url = _urlFactory.createFor<TopListEndpoint>(endpoint);
    executeRestGetStringRequest(input, output, _restService, url, TopListResponse.serializer, timeout);
  }
}

TopListResponse topListMapRestBundle(RestBundle bundle) {
  if (bundle.status != 200) {
    return TopListResponse((builder) => builder
      ..httpCode = bundle.status
      ..message = bundle.data.toString());
  }
  try {
    final jsonDecoded = {'names': jsonDecode(utf8.decode(bundle?.bodyBytes ?? 0))};
    TopListResponse response = serializers.deserializeWith(bundle.serializer, jsonDecoded);
    return response.rebuild((builder) => builder.httpCode = bundle.status);
  } catch (err) {
    logger.e('topListMapRestBundle $err');
    return TopListResponse((builder) => builder.httpCode = bundle.status);
  }
}
