import 'package:flutter/material.dart';
import 'package:name_selection_mobile/app_starter.dart';
import 'package:name_selection_mobile/injection.dart';
import 'package:name_selection_mobile/name_selection_app.dart';

Future<void> main() async {
  configureInjection(Env.prod);
  await startApp();

  runApp(NameSelectionApp());
}
