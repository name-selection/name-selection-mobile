import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:name_selection_mobile/core/core.dart';
import 'package:name_selection_mobile/core/theme/theme.dart';

import 'app_starter.dart' as starter;
import 'features/name_list/presentation/name_list_screen.dart';

class NameSelectionApp extends StatefulWidget {
  @override
  _NameSelectionAppState createState() => _NameSelectionAppState();
}

class _NameSelectionAppState extends State<NameSelectionApp> {
  @override
  Widget build(BuildContext context) {
    return ReduxProvider(
      store: starter.store,
      child: StoreConnection<AppState, AppActions, AppTheme>(
        connect: (appState) => appState.appTheme,
        builder: (ctx, appTheme, actions) => GetMaterialApp(
          navigatorKey: starter.store.state.navigationState.rootNavigatorKey,
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            Locale('ru'),
          ],
          title: 'Name Selection',
          theme: appTheme == AppTheme.light ? lightTheme(context) : darkTheme(context),
          // TODO: поменять на логин, если будет готово окно
          home: NameListScreen(
            genderType: GenderTypeEnum.male,
          ),
        ),
      ),
    );
  }
}
